#!/bin/sh

BOOKMARKS_DIR="${XDG_CONFIG_HOME:-$HOME/.config}/bookmarks"

fd . $BOOKMARKS_DIR --type=symlink -x sh -c 'printf "%25s -> %s\n" "{/}" "$(readlink -f "{}")"' | fzf | sed 's/.* -> \(.*\)/\1/'
