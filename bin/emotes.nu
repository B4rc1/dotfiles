#!/usr/bin/env nu

let convertable_file_regex = '(.*)(.jpg|.webp)$'

let dir = '/home/jonas/pix/MaiMais/'
cd $dir

# Convert Images which are not yet in pngs {
let imgs_to_convert = ls $dir | where type == file | where name =~ $convertable_file_regex | get name

$imgs_to_convert | par-each {|img|
  let new_name = $img | str replace -r $convertable_file_regex '$1.png'
  notify-send $"converting ($img) to ($new_name)"
  convert $img $new_name
  rm -fr $img
}
# }

let imgs = ls $dir | where type == file

# using https://github.com/Cloudef/dmenu-pango-imlib
# dmenu-imlib expects elements in the following format:
# IMG:<path2img>\t<text for selection>
# where \t is the escape sequence for the tab charater
let dmenu_args_list = ($imgs | where type == file | sort-by modified | each { |img| 
  $"IMG:($img.name | path expand)\t($img.name | path relative-to $dir | str substring ..(-5))"
})

let dmenu_args = $dmenu_args_list | reduce {|it, acc| $acc + "\n" + $it}
let selected = $dmenu_args | dmenu -l 9 -is 112x112 -i

if ($selected | is-empty) { exit 0 }

let file_path = $"($dir | path join $selected).png"
xclip -selection clipboard -t image/png -i $file_path
