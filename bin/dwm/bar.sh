#!/usr/bin/env sh
LANG=de_DE@euro

while [ $(date +'%S') -ne "00" ]; do
	sleep 0.1s
done

while true; do
	time=$(date +'  %a %b %d  %H:%M |')
	xsetroot -name "$time"
	sleep 60s
done 
