#!/usr/bin/env bash

layout=$(herbstclient get_attr tags.focus.tiling.focused_frame.algorithm)

[[ "$layout" -eq "vertical" ]]
case "$layout" in
  vertical)
   echo ""
 ;;
 horizontal)
  echo ""
;;
 max)
  echo ""
;;
grid)
  echo ""
;;
*)
 echo "!UNKNOWN LAYOUT!"
esac
