#!/usr/bin/env bash

if [ $(timew get dom.active) -eq 1 ]; then
  title_id=$(timew get dom.active.tag.count)
  # title=""
  # for i in $(seq $title_id); do
  #   title="$title $(timew get dom.active.tag.$i)"
  # done
  title=$(timew get dom.active.tag.1)
  t=$(timew get dom.active.duration)
  m=$(echo $t | rev | cut -d"H" -f1 | rev | cut -d"M" -f1 -s | sed -E 's/[A-Z]//g')
  h=$(echo $t | cut -d"H" -f1 -s | sed -E 's/[A-Z]//g')
  [[ -z "$m" ]] && m="0"
  [[ -z "$h" ]] && h="0"
  echo "\"$title\" for ${h}:${m}h"
else
  echo "NO ACTIVE TASK"
fi
