#!/usr/bin/env bash

LAYOUT_DIR="$HOME/.config/herbstluftwm/layouts/"

if [[ ! -d $LAYOUT_DIR ]]; then mkdir "$LAYOUT_DIR"; fi

SAVE () {
    name=$(echo "" | dmenu -p "Enter layout name")
    echo "$name"
    if [[ $name == "" ]]; then exit 0; fi

    # Saving layout
    layout=$(herbstclient dump)
    echo "herbstclient load '$layout'" > "$LAYOUT_DIR/$name"

    # Saving windows configurations
    for id in $(herbstclient foreach C clients. echo C|grep -oE '0x[0-9a-fA-F]*') ; do
        client="clients.${id}"
        current_tag="$(herbstclient get_attr tags.focus.name)"
        tag="$(herbstclient get_attr ${client}.tag)"
        if [[ "$tag" == "$current_tag" ]]; then
            rule=(
                    class="$(herbstclient get_attr ${client}.class)"
                    instance="$(herbstclient get_attr ${client}.instance)"
            )
            if herbstclient compare "${client}.floating" = on ; then
                    rule+=( "floating=on" )
                    consequence=
            else
                    rule+=(
                "index=$(herbstclient get_attr ${client}.parent_frame.index)"
                    )
            fi
            echo herbstclient rule once "${rule[@]}" "# $id" >> "$LAYOUT_DIR/$name"
            echo herbstclient apply_tmp_rule --all "${rule[@]}" "# $id" >> "$LAYOUT_DIR/$name"
        fi
    done
}

LOAD () {
    sel=$(find "$LAYOUT_DIR" -mindepth 1 -printf "%f\n" | dmenu -p "Chose a layout" -i -l 10)
    if [[ $sel == "" ]]; then exit 0; fi
    sh < "$LAYOUT_DIR/$sel"
}

case $1 in
    "save") SAVE;;
    "load") LOAD;;
    *) echo Error;;
esac
