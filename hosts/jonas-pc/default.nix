# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ inputs, config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix

      # inputs.uni-manager.nixosModules.x86_64-linux
    ];

  # services.uni-manager = {
  #   enable = true;
  #   tokenFile = "/home/jonas/tst/tst_token";
  # };

  # TODO: Move into hardware config
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # TODO: nvidia into module
  services.xserver.videoDrivers = [ "nvidia" ];
  # allow udev to recognize monitors through nvidia (used in autorandr)
  hardware.nvidia.modesetting.enable = true;

  nix.extraOptions = ''max-jobs = 2
  		       cores = 0
		     '';

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp34s0.useDHCP = true;
  networking.enableIPv6 = false;
  networking.firewall.enable = false;

  networking.nameservers = [
    "1.1.1.1" "8.8.8.8" # cloudflare google
    "2001:4860:4860::8888" # google dns
  ];

  programs.nix-ld.enable = true;
  programs.nix-ld.libraries = with pkgs; [
  ];

  services.xserver = {
    enable = true;
    dpi = 109;
    updateDbusEnvironment = true;
  };

  home.configFile = {
    "xtheme/fontconfig".source = ./config/fontconfig;
  };

  services.autorandr =
  let
    my-monitors = {
          "DP-0" = "00ffffffffffff002163122700000000151f0104a53c21783b8e05ad4f33b0260d5054afcf00d1c0d1fc81809500b300b33ca9c001016a5e00a0a0a029503020350055502100001a000000ff0030303030303030303030303031000000fd003090e6e63c010a202020202020000000fc0032374531510a2020202020202001f6020321754b909f8594041303120102112309070783010000681a00000109309000fb8180a070381f4030203500ba892100001eb8bc0050a0a0555008207800ba892100001e866f80a07038404030203500ba892100001e9ee00078a0a0325030403500ba892100001e023a801871382d40582c9600ba892100001e000000006a";
          "DP-2" = "00ffffffffffff0005e30234e44600001d200104b55021783f3ec5ad4f47a326125054bfef00d1c0b3009500818081c0316845686168e77c70a0d0a0295030203a001d4e3100001a4ed470a0d0a0465030403a001d4e3100001c000000fc005533344732473452330a202020000000fd003090dcdc50010a202020202020029f02032cf14e0103051404131f12021190595a5c230907078301000065030c002000e305e301e6060701535300d8590060a3382840a0103a101d4e3100001af4b000a0a0384d4030203a001d4e3100001a023a801871382d40582c45001d4e3100001eef51b87062a0355080b83a001d4e3100001c0000000000000000000000ce7012790000030128cffe00046f0d9f002f003f009f05450002800900b92d01046f0d9f002f003f009f05310002800900000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000a790";
    };
  in
    {
    enable = true;
    profiles = {
      "home" = {
        fingerprint = my-monitors;
        config = {
          "DP-2" = {
            enable = true;
            primary = true;
            # crtc = 0;
            mode = "3440x1440";
            position = "1440x669";
            rate = "144.0";
          };
          "DP-0" = {
            enable = true;
            primary = true;
            rotate = "left";
            # crtc = 0;
            mode = "2560x1440";
            rate = "144.0";
            position = "0x0";
          };
          # "DP-0" = {
          #   enable = true;
          #   primary = true;
          #   rotate = "left";
          #   # crtc = 0;
          #   mode = "2560x1440";
          #   rate = "144.0";
          #   position = "0x0";
          # };
        };
      };
      "kino" = {
        fingerprint = my-monitors;
        config = {
          "DP-0" = {
            enable = true;
            mode = "2560x1440";
            position = "0x0";
            rate = "144";
          };
          "DP-2" = {
            enable = true;
            primary = true;
            position = "0x0";
            rate = "144";
            mode = "3440x1440";
          };

        };
      };
    };
  };
  services.xserver.displayManager.sessionCommands = ''
    ${pkgs.autorandr}/bin/autorandr home
  '';

  environment.variables = {
    "__USES_KOY" = "1";
  };

  modules = {
    theme = {
      active = "iceberg";
    };
    desktop = {
      herbstluftwm.enable = true;
      term = {
        default = "alacritty";
        st.enable = true;
        alacritty.enable = true;
        kitty.enable = true;
      };
      browsers = {
        default = "firefox";
        firefox.enable = true;
        brave.enable = true;
      };
      gaming = {
        steam.enable = true;
      };
      media = {
        # spotify.enable = true;
        mpv.enable = true;
        documents = {
          enable = true;
          graphicsTablet.enable = true;
        };
        graphics.enable = true;
        audiobooks.enable = true;
      };
      apps = {
        keepassxc.enable = true;
        rofi.enable = true;
        mailspring.enable = true;
        discord.enable = true;
        zoom.enable = true;
        virt-manager.enable = true;
      };
    };
    services = {
      ssh.enable = true;
      syncthing.enable = true;
      gnupg.enable = true;
    };
    editors = {
      obsidian.enable = true;
      emacs.enable = true;
      neovim.enable = true;
    };
    shell = {
      bookmarks.enable = true;
      zsh.enable = true;
      git.enable = true;
      taskwarrior.enable = true;
    };
    dev = {
      java.enable = true;
      # sagemath.enable = true;
      python = {
        enable = true;
        jupyter.enable = true;
      };
      haskell.enable = true;
      rust.enable = true;
      typescript.enable = true;
      go.enable = true;
      c.enable = true;
      godot.enable = true;
      zig.enable = true;
      tex.enable = true;
    };
  };

  # networking.networkmanager.enable = true;
  user.extraGroups = [ "networkmanager" "docker" ];

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    efibootmgr

    unstable.logseq

    cheat

    libnotify # notify-send

    (luajit.withPackages (ps: with ps; [ luv ]))

    fontforge
    fontforge-gtk
    fontforge-fonttools

    obs-studio

    sqlitebrowser

    libreoffice-fresh

    vdhcoapp

    prismlauncher

    unstable.filezilla

    socat

    calibre

    eyedropper

    openconnect

    tldr

    inkscape

    poppler_utils

    ffmpeg

    hexyl

    pdftk

    openjdk8

    # aseprite-unfree

    gifski

    piper

    appimage-run

    master.anytype

    openal

    unstable.anki-bin

    copyq

    zeal

    zotero

    vscode-fhs # {
      cmake
    #}

    hwinfo

    krita

    blender_4_2

    my.pureref

    unstable.vial

    kotlin

    speedcrunch

    pinta

    unstable.figma-linux

    thunderbird

    julia

    # unstable.yuzu

    unstable.spicetify-cli

    unstable.AusweisApp2

    virt-viewer

    unstable.remmina

    filelight

    openssl

    gnome-disk-utility

    gobject-introspection

    chromium

    (pkgs.makeDesktopItem {
     name = "miro";
     desktopName = "Miro";
     exec = "${chromium}/bin/chromium --app=https://miro.com/app/dashboard/";
     genericName = "chromium";
     icon = "miro";
     })

     lutris

     zellij
     
     sageWithDoc

     # master.zed-editor

     rnote

     unstable.appflowy

     modrinth-app

     unstable.imhex

     unstable.ghidra-bin

     (unstable.osu-lazer-bin.overrideAttrs (old: rec {
      version = "2024.1115.3";
      src = fetchurl {
        url = "https://github.com/ppy/osu/releases/download/${version}/osu.AppImage";
        hash = "sha256-kwZHy0FfOUFIWvyOj0ghlQz05U+Lnzl5TgC4T6bhm7o=";
      };
     }))
  ];


  # services.firefly-iii = {
  #   enable = true;
  #   enableNginx = true;
  #   settings = {
  #     APP_KEY_FILE = /home/jonas/tst/firefly.key;
  #   };
  # };

  fonts.packages = [ pkgs.liberation_ttf ];

  # programs.zsh.interactiveShellInit = ''
  #   if [[ -z "$ZELLIJ" ]]; then
  #       if [[ "$ZELLIJ_AUTO_ATTACH" == "true" ]]; then
  #           zellij attach -c
  #       else
  #           zellij
  #       fi
  #
  #       if [[ "$ZELLIJ_AUTO_EXIT" == "true" ]]; then
  #           exit
  #       fi
  #   fi
  # '';

  services.samba = {
    enable = true;
    nmbd.enable = true;
    settings.public = {
      path = "/home/jonas/uni/semester7/windfarm-praktikum/";
      browseable = "yes";
      writable = "yes";
      public = "yes";
      "create mask" = 0644;
      "directory mask" = 0755;
      "force user" = "jonas";
    };
  };

  xdg.portal = {
    enable = true;
    xdgOpenUsePortal = true;
    extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
    config.common.default = [ "gtk" ];
  };

  services.flatpak.enable = true;

  # services.neo4j = {
  #   enable = true;
  #   bolt.tlsLevel = "DISABLED";
  #   https.enable = false;
  # };

  env.PATH = [
    "/home/jonas/uni/bin/"
  ];

  programs.nh = {
    enable = true;
    flake = "/etc/nixos";
  };

  # TODO: Module
  # VAIL Rules
  services.udev.extraRules = ''
    KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0666", TAG+="uaccess", TAG+="udev-acl"
  '';

  programs.kdeconnect.enable = true;

  services.ratbagd.enable = true;

  virtualisation.docker.enable = true;

  system.stateVersion = "23.11";
}
