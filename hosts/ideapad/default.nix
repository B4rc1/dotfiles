# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.kernelPackages = pkgs.linuxPackages_latest;
  # nix.settings.experimental-features = [ "nix-command" "flakes" ];
  
  # Bootloader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # networking.hostName = "ideapad"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";

  # Enable networking
  networking.networkmanager.enable = true;

  modules = {
    theme = {
      active = "iceberg";
    };
    desktop = {
      gaming.steam.enable = true;
      hyprland.enable = true;
      # kde.enable = true;
      term = {
        default = "kitty";
        st.enable = true;
        kitty.enable = true;
      };
      browsers = {
        default = "firefox";
        firefox.enable = true;
      };
      media = {
        spotify.enable = true;
        spotify.autostart = false;

        mpv.enable = true;
        documents.enable = true;
        graphics.enable = true;
        audiobooks.enable = true;
      };
      apps = {
        keepassxc.enable = true;
        rofi.enable = true;
        discord.enable = true;
        discord.autostart = false;
        # mailspring.enable = false; 
        # mailspring.autostart = false;
        zoom.enable = true;
      };
    };
    services = {
      ssh.enable = true;
      syncthing.enable = true;
    };
    editors = {
      obsidian.enable = true;
      neovim.enable = true;
      emacs.enable = true;
    };
    shell = {
      bookmarks.enable = true;
      zsh.enable = true;
      git.enable = true;
      lf.enable = true;
      taskwarrior.enable = true;
    };
    dev = {
      python = {
        enable = true;
        jupyter.enable = false;
      };
      godot.enable = true;
      c.enable = true;
      typescript.enable = true;
      tex.enable = true;
    };
  };

  # # Enable the X11 windowing system.
  # services.xserver.enable = true;

  # # Enable the GNOME Desktop Environment.
  # services.xserver.displayManager.gdm.enable = true;
  # services.xserver.desktopManager.gnome.enable = true;

  # Configure keymap in X11
  services.xserver = {
    layout = "de";
    xkbVariant = "nodeadkeys";
  };

  # Configure console keymap
  console.keyMap = "de-latin1-nodeadkeys";

  # Enable CUPS to print documents.
  services.printing.enable = true;

  # Enable sound with pipewire.
  sound.enable = true;
  hardware.pulseaudio.enable = false;
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;

    # use the example session manager (no others are packaged yet so this is enabled by default,
    # no need to redefine it in your config for now)
    #media-session.enable = true;
  };


  # Enable automatic login for the user.
  services.xserver.displayManager.autoLogin.enable = true;
  services.xserver.displayManager.autoLogin.user = "jonas";

  # Workaround for GNOME autologin: https://github.com/NixOS/nixpkgs/issues/103746#issuecomment-945091229
  systemd.services."getty@tty1".enable = false;
  systemd.services."autovt@tty1".enable = false;

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
	  rnote
    gnomeExtensions.taskwhisperer
    graphene
  ];

  system.stateVersion = "23.05";
}
