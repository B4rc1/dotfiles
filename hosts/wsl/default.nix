# Edit this configuration file to define what should be installed on
# your system. Help is available in the configuration.nix(5) man page, on
# https://search.nixos.org/options and in the NixOS manual (`nixos-help`).

# NixOS-WSL specific options are documented on the NixOS-WSL repository:
# https://github.com/nix-community/NixOS-WSL

{ config, lib, pkgs, inputs, ... }:

{
  imports = [
    # include NixOS-WSL modules
    # <nixos-wsl/modules>
    inputs.nixos-wsl.nixosModules.wsl
  ];

  nix.extraOptions = ''max-jobs = 3
  		       cores = 4
		     '';

  environment.variables = {
    "__USES_KOY" = "1";
  };

  environment.systemPackages = with pkgs; [
    exercism

    texliveFull texlab

  ];

  networking.hostName = "wsl";

  wsl.enable = true;
  wsl.defaultUser = "jonas";
  # wsl.wslConf.user.default = "jonas";

  modules = {
    theme.active = "iceberg";
    desktop.media.documents.enable = true;
    editors.neovim = {
      enable = true;
      lspconfig.servers = [
        { name = "texlab"; }
      ];
    };
    shell = {
      zsh.enable = true;
      git.enable = true;
      taskwarrior.enable = true;
    };
    dev = {
      godot.enable = true;
      zig.enable = true;
      python.enable = true;
      rust.enable = true;
      haskell.enable = true;
      c.enable = true;
    };
  };

  boot.loader.systemd-boot.enable = false;
  networking.firewall.enable = false;

  system.stateVersion = "23.11"; # Did you read the comment?
}
