# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # graphical bootup screen
  # boot.initrd.systemd.enable = true;
  boot.plymouth = {
    enable = true;
    # theme = "breeze";
  };

  # services.xserver.displayManager.autoLogin = {
  #   enable = true;
  #   user = config.user.name;
  # };

  services.tlp.enable = true;

  nix.extraOptions = ''max-jobs = 2
  		       cores = 2
		     '';

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  networking.useDHCP = false;
  networking.interfaces.enp0s31f6.useDHCP = true;
  networking.interfaces.wlp4s0.useDHCP = true;
  networking.enableIPv6 = false;

  networking.networkmanager.enable = true;
  user.extraGroups = [ "networkmanager" ];

  services.xserver = {
    enable = true;
    libinput = {
      enable = true;
      touchpad = {
        tapping = true;
        naturalScrolling = true;
        middleEmulation = true;
      };
    };
  };

  home.configFile = {
    "xtheme/fontconfig".source = ./config/fontconfig;
  };

  modules = {
    theme = {
      active = "iceberg";
    };
    desktop = {
      gaming.steam.enable = true;
      kde.enable = true;
      term = {
        default = "kitty";
        st.enable = true;
        kitty.enable = true;
      };
      browsers = {
        default = "firefox";
        firefox.enable = true;
      };
      media = {
        spotify.enable = true;
        spotify.autostart = false;

        mpv.enable = true;
        documents.enable = true;
        graphics.enable = true;
        audiobooks.enable = true;
      };
      apps = {
        keepassxc.enable = true;
        rofi.enable = true;
        discord.enable = true;
        discord.autostart = false;
        mailspring.enable = true;
        mailspring.autostart = false;
        zoom.enable = true;
      };
    };
    services = {
      ssh.enable = true;
      syncthing.enable = true;
    };
    editors = {
      obsidian.enable = true;
      neovim.enable = true;
      emacs.enable = true;
    };
    shell = {
      bookmarks.enable = true;
      zsh.enable = true;
      git.enable = true;
      taskwarrior.enable = true;
    };
    dev = {
      python = {
        enable = true;
        jupyter.enable = false;
      };
      godot.enable = true;
      c.enable = true;
      typescript.enable = true;
    };
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    efibootmgr
    networkmanager_dmenu networkmanagerapplet
    # logseq
    chromium
    # FIXME: you need to run 'path/to/net.downloadhelper.coapp install --user' right now after installing it.
    my.vdhcoapp

    libreoffice-still

    gnome.simple-scan

    unstable.foot

    libnotify

    pdftk

    gltron

    unstable.vial
  ];

  # VAIL Rules
  services.udev.extraRules = ''
    KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0666", TAG+="uaccess", TAG+="udev-acl"
  '';

  systemd.extraConfig = ''
    DefaultTimeoutStopSec=7s
    '';

}
