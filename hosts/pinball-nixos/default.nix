# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # bootloader
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";

  # TODO: nvidia into module
  services.xserver.videoDrivers = [ "nvidia" ];

  services.xserver.displayManager.autoLogin = {
    enable = true;
    user = config.user.name;
  };

  nix.extraOptions = ''max-jobs = 3
  		       cores = 3
		     '';

  # Set your time zone.
  time.timeZone = "Europe/Berlin";


  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.networkmanager.enable = true;
  networking.firewall.enable = false;
  networking.enableIPv6 = false;
  networking.nameservers = [
    "1.1.1.1" "8.8.8.8" # cloudflare google
    "2001:4860:4860::8888" # google dns
  ];

  services.xserver.enable = true;

  virtualisation.docker.enable = true;
  virtualisation.docker.enableNvidia = true;

  user.extraGroups = [ "docker" "libvirt" ];

  environment.variables = {
    "__USES_KOY" = "1";
  };

#  home.configFile = {
#    "xtheme/fontconfig".source = ./config/fontconfig;
#  };

  modules = {
    theme = {
      active = "iceberg";
    };
    desktop = {
      # cinnamon.enable = true;
      # xmonad.enable = true;
      herbstluftwm.enable = true;
      # hyprland.enable = true;
      term = {
        default = "wezterm";
        alacritty.enable = true;
        st.enable = true;
        kitty.enable = true;
      };
      browsers = {
        default = "firefox";
        firefox.enable = true;
        brave.enable = true;
      };
      gaming = {
        steam.enable = true;
      };
      media = {
        spotify.enable = true;
        mpv.enable = true;
        documents = {
          enable = true;
          graphicsTablet.enable = true;
        };
        graphics.enable = true;
        audiobooks.enable = true;
      };
      apps = {
        keepassxc.enable = true;
        rofi.enable = true;
        mailspring.enable = true;
        discord.enable = true;
        # zoom.enable = true;
        virt-manager.enable = true;
      };
    };
    services = {
      ssh.enable = true;
      syncthing.enable = true;
      gnupg.enable = true;
    };
    editors = {
      obsidian.enable = true;
      # emacs.enable = true;
      neovim.enable = true;
    };
    shell = {
      bookmarks.enable = true;
      zsh.enable = true;
      git.enable = true;
      taskwarrior.enable = true;
    };
    dev = {
      java.enable = true;
      # sagemath.enable = true;
      # python = {
      #  enable = true;
      #  jupyter.enable = true;
      # };
      haskell.enable = true;
      rust.enable = true;
      # prolog.enable = true;
      # typescript.enable = true;
      go.enable = true;
      c.enable = true;
      godot.enable = true;
    };
  };

  fonts.packages = with pkgs; [
    (nerdfonts.override { fonts = [ "Meslo" ]; })
  ];

  services.earlyoom = {
    enable = true;
    freeMemThreshold = 2;
  };

  programs.kdeconnect.enable = true;


  env.PATH = [
    "/home/jonas/uni/bin/"
  ];

  environment.systemPackages = with pkgs; [
    efibootmgr

    # unstable.polymc

    python3Packages.pip

    # nasc

    # unstable.aseprite-unfree

    docker-compose

    pinta

    gnuplot

    unstable.lapce

    unstable.cryptomator

    linuxKernel.packages.linux_zen.perf
    perf-tools

    # libsForQt5.kleopatra

    libreoffice-still

    (appimage-run.override {
      extraPkgs = p: [ p.libsecret ];
    })

    # unstable.anytype

    krita

    unstable.remmina
    chromium

    dos2unix

    unstable.prismlauncher

    speedcrunch

    unstable.wezterm

    unstable.aseprite

    blender_3_6

    libsForQt5.dolphin

    lxqt.lxqt-qtplugin
    lxqt.lxqt-config
    libsForQt5.qtstyleplugin-kvantum

    deepin.dde-file-manager
  ];

  # environment.variables = {
  #   GTK_USE_PORTAL = "1";
  #   QT_QPA_PLATFORMTHEME = "lxqt";
  # };

  user.packages = [

  ];


}
