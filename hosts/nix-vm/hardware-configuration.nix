# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/profiles/qemu-guest.nix")
    ];

  boot.initrd.availableKernelModules = [ "ahci" "xhci_pci" "virtio_pci" "sr_mod" "virtio_blk" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ ];
  boot.extraModulePackages = [ ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/5cad53cf-46b0-4c1b-9d20-7ba92d947b51";
      fsType = "ext4";
    };
  fileSystems."/mnt" =
    { device = "/vmshare";
      fsType = "9p";
      options = [ "trans=virtio" ];
    };

  # swapDevices =
  #   [ { device = "/dev/disk/by-uuid/3099ba26-f3b6-4a65-9c8b-b1d54d436b22"; }
  #   ];

}
