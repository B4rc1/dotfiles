# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  boot.loader.grub.device = "/dev/vda"; # or "nodev" for efi only

  # Set your time zone.
  time.timeZone = "Europe/Berlin";

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;

  # Enable the X11 windowing system.
  services.xserver = {
    enable = true;
    /* dpi = 65; */
    resolutions = [ { "x" = 1920; "y" = 1080; } ];
  };

  # TODO: fontconfig
  /* fonts.fontconfig. */
  modules = {
    desktop = {
      /* dwm.enable = true; */
      herbstluftwm.enable = true;
    };
    editors = {
      neovim.enable = true;
    };
    shell = {
      bookmarks.enable = true;
      zsh.enable = true;
    };
  };



  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
  #   wget
    firefox
    git
  ];
}
