{
  inputs =
    {
      nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.11";
      nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
      nixpkgs-master.url = "github:NixOS/nixpkgs/master";
      home-manager.url = "github:nix-community/home-manager/release-24.11";
      home-manager.inputs.nixpkgs.follows = "nixpkgs";
      agenix.url = "github:ryantm/agenix";
      agenix.inputs.nixpkgs.follows = "nixpkgs";

      # uni-manager.url = "path:/home/jonas/nerdshit/python/uni-manager-bot";

      # Windows Subsystem for Linux
      nixos-wsl.url = github:nix-community/NixOS-WSL;
      nixos-wsl.inputs.nixpkgs.follows = "nixpkgs";
      # nixos-wsl.inputs.flake-utils.follows = "flake-utils";

      pypi-deps-db = {
        url = "github:DavHau/pypi-deps-db";
        inputs = {
          nixpkgs.follows = "nixpkgs";
          mach-nix.follows = "mach-nix";
        };
      };

      mach-nix = {
        url = "mach-nix/3.5.0";
        inputs = {
          nixpkgs.follows = "nixpkgs";
          pypi-deps-db.follows = "pypi-deps-db";
        };
      };

      blender-bin.url = "github:edolstra/nix-warez?dir=blender";

      # --- My patched Applications ---
      st.url = "git+https://codeberg.org/B4rc1/st.git";
      dwm.url = "git+https://codeberg.org/B4rc1/dwm.git?ref=my-dwm";
      sxiv.url = "git+https://codeberg.org/B4rc1/sxiv";
    };

  outputs = inputs @ { self, nixpkgs, nixpkgs-unstable, nixpkgs-master, mach-nix, blender-bin, ... }:
    let
      inherit (lib.my) mapModules mapModulesRec mapHosts;

      system = "x86_64-linux";

      mkPkgs = pkgs: extraOverlays: import pkgs {
        inherit system;
        config.allowUnfree = true;  # forgive me Stallman senpai
        config.permittedInsecurePackages = [
          "electron-27.3.11" # logseq
          "dotnet-runtime-6.0.36" # OpenTabletDriver
          "dotnet-sdk-wrapped-6.0.428"
          "dotnet-sdk-6.0.428"
        ];
        config.android_sdk.accept_license = true;
        overlays = extraOverlays ++ (lib.attrValues self.overlays);
      };
      pkgs  = mkPkgs nixpkgs [ self.overlay inputs.dwm.overlay inputs.sxiv.overlay blender-bin.overlays.default ];
      pkgs' = mkPkgs nixpkgs-unstable [];
      pkgs''= mkPkgs nixpkgs-master [];

      lib = nixpkgs.lib.extend
       (self: super: { my = import ./lib { inherit pkgs inputs; lib = self; }; });
  in {
    # lib = lib.my;

    nixosConfigurations =
      mapHosts ./hosts {};

    devShell."${system}" =
      import ./shell.nix { inherit pkgs; };

    overlays =
      mapModules ./overlays import;

    packages."${system}" =
      mapModules ./packages (p: pkgs.callPackage p {});

    overlay =
      final: prev: {
        unstable = pkgs';
        master = pkgs'';
        my = self.packages."${system}";
      };

    # Unify all references to nixpkgs to this version
    nix.registry.nixpkgs.flake = nixpkgs;

    templates = import ./templates;

    nix.nixPath =
        let
    path = toString ./.;
  in
    [ "repl=${path}/repl.nix" "nixpkgs=${inputs.nixpkgs}" ];
  };
}
