#                   ██
#                  ░██
#    ██████  ██████░██      ██████  █████
#   ░░░░██  ██░░░░ ░██████ ░░██░░█ ██░░░██
#      ██  ░░█████ ░██░░░██ ░██ ░ ░██  ░░
#     ██    ░░░░░██░██  ░██ ░██   ░██   ██
# ██ ██████ ██████ ░██  ░██░███   ░░█████
# ░░ ░░░░░░ ░░░░░░  ░░   ░░ ░░░     ░░░░░



# ▄▖▜     ▘
# ▙▌▐ ▌▌▛▌▌▛▌▛▘
# ▌ ▐▖▙▌▙▌▌▌▌▄▌
#       ▄▌
loadplugs() {
		source ~/.zplug/init.zsh

		# Git plugin (ga, glo, gi, gd, grh, gss, gclean, gcp)
		zplug 'wfxr/forgit'
		zplug "zsh-users/zsh-syntax-highlighting"
		zplug "zsh-users/zsh-history-substring-search"
		zplug "zsh-users/zsh-autosuggestions"
		# Prompt
		zplug romkatv/powerlevel10k, as:theme, depth:1

		# Install plugins if there are plugins that have not been installed
		if ! zplug check --verbose; then
		    printf "Install? [y/N]: "
		    if read -q; then
		        echo; zplug install
		    fi
		fi

		# Then, source plugins and add commands to $PATH
		zplug load
}


# Only load plugins and prompt if interactive
if [[ -o interactive ]]; then
	# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
	# Initialization code that may require console input (password prompts, [y/n]
	# confirmations, etc.) must go above this block; everything else may go below.
	if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
		source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
	fi
	loadplugs
fi


source $HOME/.profile
export _JAVA_AWT_WM_NONREPARENTING=1

# Path to your oh-my-zsh installation.
export ZSH="/home/jonas/.oh-my-zsh"

# plugins=(git zsh-autosuggestions history-substring-search zsh-syntax-highlighting)

export ZSH_AUTOSUGGEST_STRATEGY=(match_prev_cmd completion)
export ZSH_AUTOSUGGEST_USE_ASYNC=true
# dont suggest anythin 50 characters or longer
export ZSH_AUTOSUGGEST_HISTORY_IGNORE="?(#c50,)"
# export ZSH_AUTOSUGGEST_USE_ASYNC=1

source $ZSH/oh-my-zsh.sh

# Ctrl + Space as suggest accept
bindkey '^ ' autosuggest-accept
# unmap Ctrl + x and replace by kill-word
bindkey -per '^X'
bindkey '^X' kill-word


export LANG=en_US.UTF-8
export EDITOR='nvim'

alias p="doas pacman" \
    pi="doas pacman -S"   \
    pu="paru -Syyu"       \
    pr="doas pacman -Rs"  \
alias clk="tty-clock -s" 
alias r=". ranger"
alias figlet='figlet -f ~/.config/figlet/3d.flf'
alias o="xdg-open"
alias xclip="xclip -selection clipboard"

alias \
	v='nvim' \
	suvi='doas nvim' \
	l='exa -la' \
	ls="exa" \
	ll="exa -l" 

alias lf="~/.local/bin/lf.sh"
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
compdef config="git" # autocompletion

alias urldecode='python3 -c "import sys, urllib.parse as ul; \
    print(ul.unquote_plus(sys.argv[1]))"'
alias urlencode='python3 -c "import sys, urllib.parse as ul; \
    print (ul.quote_plus(sys.argv[1]))"'

export LESSOPEN="| src-hilite-lesspipe.sh %s"
export LESS=' -R '

LFCD="/usr/share/lf/lfcd.sh"
if [ -f "$LFCD" ]; then
    source "$LFCD"
fi
bindkey -s '^o' 'lfcd\n'





# pretty man pages
man() {
    LESS_TERMCAP_md=$'\e[01;34m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    # LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}

alias pacs='pacman -Slq | fzf --multi --preview "pacman -Si {1}" | xargs -ro doas pacman -S'

alias todo="/home/jonas/.conky/todo/todo.sh"


source /usr/share/fzf/key-bindings.zsh
source /usr/share/fzf/completion.zsh

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
if [ -e /home/jonas/.nix-profile/etc/profile.d/nix.sh ]; then . /home/jonas/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
