(prev: final: {
    dmenu = final.dmenu.overrideAttrs (oldAttrs: rec {
      src = prev.fetchFromGitHub {
        owner = "Cloudef";
        repo = "dmenu-pango-imlib";
        rev = "a9066ec7e5df7153e67544d3ef9439b518404346";
        sha256 = "sha256-cWDO/0zP5QiSU1eHRUsDxo4F1i4ACnP9vWBGuqqjYs4=";
      };
      buildInputs = with prev; oldAttrs.buildInputs ++  [
        imlib2
        pango
        openssl
        xorg.libXft
        pkg-config ];
      preConfigure = ''
        sed -i "s@PREFIX = /usr/local@PREFIX = $out@g" config.mk
      '';
    });
})
