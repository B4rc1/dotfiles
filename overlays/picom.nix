(prev: final: {
    picom = final.picom.overrideAttrs (oldAttrs: rec {
			version = "v7";
      src = prev.fetchFromGitHub {
				owner = "jonaburg";
				repo = "picom";
				rev = "a8445684fe18946604848efb73ace9457b29bf80";
				sha256 = "R+YUGBrLst6CpUgG9VCwaZ+LiBSDWTp0TLt1Ou4xmpQ=";
				fetchSubmodules = true;
      };
			# upstream depends on pcre2 but this fork only works with pcre1.
			buildInputs = oldAttrs.buildInputs ++ [ prev.pcre ];
			nativeBuildInputs = oldAttrs.nativeBuildInputs ++ [ prev.asciidoc ];
    });
})
