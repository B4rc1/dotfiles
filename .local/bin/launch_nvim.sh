#!/bin/sh

if [[ -S $NVIM_LISTEN_ADDRESS ]]; then
    nvr "$@"
else
    st -e nvr "$@"
fi
