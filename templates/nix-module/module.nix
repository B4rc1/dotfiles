{ config, system, options, lib, pkgs, ... }:

with lib;

let
  cfg = config.X.Y;
in {
  options.X.Y = {
    enable = mkEnableOption "Y";
  };

  config = lib.mkIf cfg.enable {
  };
}
