{
  python = {
    description = "Python Project";
    path = ./python;
  };

  nix-module = {
    description = "A nix module";
    path = ./nix-module;
  };

  cpp = {
    description = "C++ Flake template";
    path = ./cpp;
  };
}
