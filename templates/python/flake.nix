{
  description = "my project description";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };

        python-with-my-packages = pkgs.python3.withPackages (p: with p; [
          # dependencies here

          autopep8
        ]);
        # pkg = pkgs.python3Packages.buildPythonApplication {
        # };
      in
      {
        devShells.default = pkgs.mkShell {
          buildInputs = [
            python-with-my-packages
          ];

          shellHook = ''
            export PYTHONPATH=${python-with-my-packages}/${python-with-my-packages.sitePackages}
          '';
        };
      });

      # package.default = pkg;

      # apps.default = {
      #   type = "app";
      #   program = "${self.package.${system}.default}/bin/main.py";
      # };

      # nixosModules = { config, ... }: {
      #   imports = [ (import ./module.nix { inherit pkg; }) ];
      # };
}
