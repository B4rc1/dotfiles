# Remap hjkl -> dtrn
bindkey -M vicmd "H" vi-kill-eol
bindkey -M vicmd "T" vi-join
bindkey -M vicmd "L" vi-rev-repeat-search
bindkey -M vicmd "K" vi-replace
bindkey -M vicmd "J" vi-find-prev-char-skip
bindkey -M vicmd "h" vi-delete
bindkey -M vicmd "d" vi-backward-char
bindkey -M vicmd "t" down-line-or-history
bindkey -M vicmd "r" up-line-or-history
bindkey -M vicmd "n" vi-forward-char
bindkey -M vicmd "l" vi-repeat-search
bindkey -M vicmd "k" vi-replace-chars
bindkey -M vicmd "j" vi-find-next-char-skip

bindkey -M visual "t" down-line
bindkey -M visual "r" up-line

bindkey -M viopp "t" down-line
bindkey -M viopp "r" up-line
