# zgen
# we handle compinit ourselves...
export ZGEN_AUTOLOAD_COMPINIT=0

# [fzf] fd > find
if (( $+commands[fd] )); then
  export FZF_DEFAULT_OPTS="--reverse --ansi"
  export FZF_DEFAULT_COMMAND="fd ."
  export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
  export FZF_ALT_C_COMMAND="fd -t d . $HOME"
fi

unsetopt BEEP                    # shush, be quiet.
setopt IGNOREEOF                 # don't exit on <CTRL-D>

## History
HISTFILE="$XDG_CACHE_HOME/zhistory"
export HISTSIZE=999999999
export SAVEHIST=$HISTSIZE

# setopt EXTENDED_HISTORY          # Write the history file in the ':start:elapsed;command' format.
setopt APPEND_HISTORY            # Appends history to history file on exit
setopt INC_APPEND_HISTORY        # Write to the history file immediately, not when the shell exits.
setopt HIST_IGNORE_ALL_DUPS      # Remove old events if new event is a duplicate
setopt HIST_REDUCE_BLANKS        # Minimize unnecessary whitespace
setopt HIST_IGNORE_DUPS          # Do not record an event that was just recorded again.
setopt HIST_IGNORE_SPACE         # Do not record an event starting with a space.
setopt HIST_VERIFY               # Do not execute immediately upon history expansion.

## Plugins
export ZSH_AUTOSUGGEST_STRATEGY=(history completion)
export ZSH_AUTOSUGGEST_USE_ASYNC=true
export ZSH_AUTOSUGGEST_HISTORY_IGNORE="?(#c50,)"

export YSU_MESSAGE_POSITION="after"



## Directories
DIRSTACKSIZE=9
unsetopt AUTO_CD            # Implicit CD slows down zsh-autocomplete
setopt AUTO_PUSHD           # Push the old directory onto the stack on cd.
setopt PUSHD_IGNORE_DUPS    # Do not store duplicates in the stack.
setopt PUSHD_SILENT         # Do not print the directory stack after pushd or popd.
setopt PUSHD_TO_HOME        # Push to home directory when no argument is given.
