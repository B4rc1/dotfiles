if command -v exa >/dev/null; then
  alias ls="exa --group-directories-first --icons --hyperlink";
  alias l="exa --group-directories-first -lg --icons --hyperlink";
  alias ll="exa --group-directories-first -lga --icons --hyperlink";
  alias tree="exa --group-directories-first --tree --icons --hyperlink"
fi

if command -v fasd >/dev/null; then
  alias v="f -e nvim";
fi

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias mkdir='mkdir -pv'

alias o="xdg-open"

alias y='xclip -selection clipboard -in'
alias p='xclip -selection clipboard -out'

alias path='echo -e ${PATH//:/\\n}'
alias ports='netstat -tulanp'
