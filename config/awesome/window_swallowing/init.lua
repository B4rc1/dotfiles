-- shamelessly stolen from/inspired by: https://github.com/BlingCorp/bling
local awful = require("awful")

local lib = require("lib")
local helpers = require("window_swallowing.helpers")

-- It might actually swallow too much, that's why there is a filter option by classname
-- without the don't-swallow-list it would also swallow for example
-- file pickers or new firefox windows spawned by an already existing one

local window_swallowing_activated = false

-- you might want to add or remove applications here
local whitelist = { "xst" }

-- check if element exist in table
-- returns true if it is

-- if the swallowing filter is active checks the child and parent classes
-- against their filters
local function check_swallow(parent)
  return lib.is_in_table(parent, whitelist)
end

-- async function to get the parent's pid
-- recieves a child process pid and a callback function
-- parent_pid in format "init(1)---ancestorA(pidA)---ancestorB(pidB)...---process(pid)"
local function get_parent_pid(child_ppid, callback)
    local ppid_cmd = string.format("pstree -A -p -s %s", child_ppid)
    awful.spawn.easy_async(ppid_cmd, function(stdout, stderr, reason, exit_code)
        -- primitive error checking
        if stderr and stderr ~= "" then
            callback(stderr)
            return
        end
        local ppid = stdout
        callback(nil, ppid)
    end)
end


-- the function that will be connected to / disconnected from the spawn client signal
local function manage_clientspawn(c)
    -- get the last focused window to check if it is a parent window
    local parent_client = awful.client.focus.history.get(c.screen, 1)
    if not parent_client then
        return
    elseif parent_client.type == "dialog" or parent_client.type == "splash" then
        return
    end

    get_parent_pid(c.pid, function(err, ppid)
        if err then
            return
        end
        parent_pid = ppid
    if
        -- will search for "(parent_client.pid)" inside the parent_pid string
        ( tostring(parent_pid):find("("..tostring(parent_client.pid)..")") )
        and check_swallow(parent_client.class)
    then
        c:connect_signal("unmanage", function()
            if parent_client then
                helpers.turn_on(parent_client)
                helpers.sync(parent_client, c)
            end
        end)

        helpers.sync(c, parent_client)
        helpers.turn_off(parent_client)
    end
    end)
end

-- without the following functions that module would be autoloaded by require("bling")
-- a toggle window swallowing hotkey is also possible that way

local function enable()
    client.connect_signal("manage", manage_clientspawn)
    window_swallowing_activated = true
end

local function disable()
    client.disconnect_signal("manage", manage_clientspawn)
    window_swallowing_activated = false
end

local function toggle()
    if window_swallowing_activated then
        enable()
    else
        disable()
    end
end

return {
    enable = enable,
    disable = disable,
    toggle = toggle,
}
