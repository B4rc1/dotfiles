-- Imports {{{
-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")


-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")

-- my lib functions
local lib = require("lib")

local window_swallowing = require("window_swallowing")
local on_empty_keys = require("on_empty_keys")

--- }}}

-- Error handling {{{
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
	naughty.notify({ preset = naughty.config.presets.critical,
		title = "Oops, there were errors during startup!",
		text = awesome.startup_errors })
end


-- Handle runtime errors after startup
do
	local in_error = false
	awesome.connect_signal("debug::error", function (err)
		-- Make sure we don't go into an endless error loop
		if in_error then return end
		in_error = true

		naughty.notify({ preset = naughty.config.presets.critical,
			title = "Oops, an error happened!",
			text = tostring(err) })
		in_error = false
	end)
end
-- }}}

-- Variable definitions {{{
-- setup randomness
math.randomseed(os.time());

-- Themes define colours, icons, font and wallpapers.
beautiful.init("~/.config/awesome/theme.lua")

window_swallowing.enable()

modkey = "Mod4"
terminal = "xst"

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
	awful.layout.suit.tile,
	awful.layout.suit.floating,
	awful.layout.suit.tile.left,
	awful.layout.suit.tile.bottom,
	awful.layout.suit.tile.top,
	awful.layout.suit.fair,
	awful.layout.suit.fair.horizontal,
	awful.layout.suit.spiral,
	awful.layout.suit.spiral.dwindle,
	awful.layout.suit.max,
	awful.layout.suit.max.fullscreen,
	awful.layout.suit.magnifier,
	awful.layout.suit.corner.nw,
	-- awful.layout.suit.corner.ne,
	-- awful.layout.suit.corner.sw,
	-- awful.layout.suit.corner.se,
}
-- }}}


-- {{{ Wibar
-- Create a textclock widget
mytextclock = wibox.widget.textclock()

-- Create a wibox for each screen and add it
local taglist_buttons = gears.table.join(
	awful.button({ }, 1, function(t) t:view_only() end),
	awful.button({ modkey }, 1, function(t)
		if client.focus then
			client.focus:move_to_tag(t)
		end
	end),
	awful.button({ }, 3, awful.tag.viewtoggle),
	awful.button({ modkey }, 3, function(t)
		if client.focus then
		client.focus:toggle_tag(t)
		end
	end),
	awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),
	awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

local tasklist_buttons = gears.table.join(
	awful.button({ }, 1, function (c)
	if c == client.focus then
			c.minimized = true
		else
			c:emit_signal(
				"request::activate",
				"tasklist",
				{raise = true}
			)
		end
	end),
	awful.button({ }, 3, function()
	awful.menu.client_list({ theme = { width = 250 } })
	end),
	awful.button({ }, 4, function ()
		awful.client.focus.byidx(1)
	end),
	awful.button({ }, 5, function ()
		awful.client.focus.byidx(-1)
end))



function set_wallpaper(s)
	-- random Wallpaper
	local dir = "/home/jonas/pix/wal/"
	local wallpaperList = {}
	local command = [[find "]] .. dir.. [[" -type f]]

	awful.spawn.easy_async_with_shell(command, function(stdout)
		for file in stdout:gmatch("[^\n]+") do
			table.insert(wallpaperList, file)
		end

		gears.wallpaper.maximized(wallpaperList[math.random(1, #wallpaperList)], s, false)
	end)
end

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", set_wallpaper)

awful.screen.connect_for_each_screen(function(s)
	-- Wallpaper
	set_wallpaper(s)

	-- Each screen has its own tag table.
	awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s, awful.layout.layouts[1])


	-- Create a promptbox for each screen
	s.mypromptbox = awful.widget.prompt()
	-- Create an imagebox widget which will contain an icon indicating which layout we're using.
	-- We need one layoutbox per screen.
	s.mylayoutbox = awful.widget.layoutbox(s)
	s.mylayoutbox:buttons(gears.table.join(
		awful.button({ }, 1, function () awful.layout.inc( 1) end),
		awful.button({ }, 3, function () awful.layout.inc(-1) end),
		awful.button({ }, 4, function () awful.layout.inc( 1) end),
		awful.button({ }, 5, function () awful.layout.inc(-1) end)
	))

	-- Create a taglist widget
	s.mytaglist = awful.widget.taglist {
		screen	= s,
		filter	= awful.widget.taglist.filter.all,
		buttons = taglist_buttons
	}

	-- Create a tasklist widget
	s.mytasklist = awful.widget.tasklist {
		screen	= s,
		filter	= awful.widget.tasklist.filter.currenttags,
		buttons = tasklist_buttons
	}

	-- Create the wibox
	s.mywibox = awful.wibar({ position = "top", screen = s, visible = s == screen.primary })

	-- Add widgets to the wibox
	s.mywibox:setup {
		layout = wibox.layout.align.horizontal,
		{ -- Left widgets
			layout = wibox.layout.fixed.horizontal,
			s.mytaglist,
			s.mylayoutbox,
			s.mypromptbox,
		},
		s.mytasklist, -- Middle widget
		{ -- Right widgets
			layout = wibox.layout.fixed.horizontal,
			wibox.widget.systray(),
			mytextclock,
		},
	}

  -- {{{ smart borders
  s:connect_signal("arrange",
    function ()
      local clients = s.tiled_clients
      local layout  = s.selected_tag.name

      if #clients > 0 then
        for _, c in pairs(clients) do
          if layout == "floating" then
            c.border_width = beautiful.border_width

          elseif #clients == 1 or layout == "max" then
            c.border_width = 0
          else
            c.border_width = beautiful.border_width
          end
        end
      end
    end)
  -- }}}

	s:connect_signal("swapped", function(self, other, is_source)
    if not is_source then return end

    local st = self.selected_tag
    local sc = st:clients()
    local ot = other.selected_tag
    local oc = ot:clients()

    for _, c in ipairs(sc) do
      c:move_to_tag(ot)
      ot:emit_signal("tagged", c)
    end

    for _, c in ipairs(oc) do
      c:move_to_tag(st)
      st:emit_signal("tagged", c)
    end
	end)
end)
-- }}}

-- {{{ Mouse bindings
root.buttons(gears.table.join(
	-- awful.button({ }, 3, function () mymainmenu:toggle() end),
	awful.button({ }, 4, awful.tag.viewnext),
	awful.button({ }, 5, awful.tag.viewprev)
))

-- }}}



root.keys(require("keybindings").globalkeys)

on_empty_keys.enable()

-- {{{ Rules
-- local function setTitlebar(client, s)
-- 	if s then
-- 		if client.titlebar == nil then
-- 			client:emit_signal("request::titlebars", "rules", {})
-- 		end
-- 		awful.titlebar.show(client)
-- 	else 
-- 		awful.titlebar.hide(client)
-- 	end
-- end
--
-- --Toggle titlebar on floating status change
-- client.connect_signal("property::floating", function(c)
-- 	setTitlebar(c, c.floating)
-- end)
--
-- -- Hook called when a client spawns
-- client.connect_signal("manage", function(c) 
-- 	setTitlebar(c, c.floating or c.first_tag.layout == awful.layout.suit.floating)
-- end)
--
-- -- Show titlebars on tags with the floating layout
-- tag.connect_signal("property::layout", function(t)
-- 	for _, c in pairs(t:clients()) do
-- 		if t.layout == awful.layout.suit.floating then
-- 			setTitlebar(c, true)
-- 		else
-- 			setTitlebar(c, false)
-- 		end
-- 	end
-- end)




-- Rules to apply to new clients (through the "manage" signal).
awful.rules.rules = require("rules")
-- }}}

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
	-- Set the windows at the slave,
	-- i.e. put it at the end of others instead of setting it master.
	if not awesome.startup then awful.client.setslave(c) end

	if awesome.startup
		and not c.size_hints.user_position
		and not c.size_hints.program_position then
		-- Prevent clients from being unreachable after screen count changes.
		awful.placement.no_offscreen(c)
	end

  if c.transient_for then
    awful.placement.centered(c, {parent = c.transient_for})
  end
end)

-- Add a titlebar if titlebars_enabled is set to true in the rules.
client.connect_signal("request::titlebars", function(c)
	-- buttons for the titlebar
	local buttons = gears.table.join(
		awful.button({ }, 1, function()
			c:emit_signal("request::activate", "titlebar", {raise = true})
			awful.mouse.client.move(c)
		end),
		awful.button({ }, 3, function()
			c:emit_signal("request::activate", "titlebar", {raise = true})
			awful.mouse.client.resize(c)
		end)
	)

	awful.titlebar(c) : setup {
		{ -- Left
			awful.titlebar.widget.iconwidget(c),
			buttons = buttons,
			layout	= wibox.layout.fixed.horizontal
		},
		{ -- Middle
			{ -- Title
				align  = "center",
				widget = awful.titlebar.widget.titlewidget(c)
			},
			buttons = buttons,
			layout	= wibox.layout.flex.horizontal
		},
		{ -- Right
			awful.titlebar.widget.floatingbutton (c),
			awful.titlebar.widget.maximizedbutton(c),
			awful.titlebar.widget.stickybutton	 (c),
			awful.titlebar.widget.ontopbutton		 (c),
			awful.titlebar.widget.closebutton		 (c),
			layout = wibox.layout.fixed.horizontal()
		},
		layout = wibox.layout.align.horizontal
	}
end)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
	c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

-- }}}

client.connect_signal("focus", function(c) 
  c.border_color = beautiful.border_focus
end)

client.connect_signal("unfocus", function(c)
  c.border_color = beautiful.border_normal
end)
-- }}}


-- Auto-run
local function run_once(cmd_arr)
    for _, cmd in ipairs(cmd_arr) do
        local findme = cmd
        local firstspace = cmd:find(" ")
        if firstspace then
            findme = cmd:sub(0, firstspace-1)
        end
        -- FIXME: nixos renames some programs. eg:
        --          Discord -> .Discord-wrappe
        --          spotify -> .spotify-wrapp
        --          volctl  -> ..volctl-wrappe
        --        this is not properly handled here.
        --        what i will probably do is properly implement autostart again in nix
        awful.spawn.with_shell(string.format("pgrep -u $USER -x ..%s-wrappe > /dev/null || (%s)", findme, 
cmd))
    end
end
run_once({"volctl"})
