local awful = require("awful")
local gears = require("gears")

local on_empty_keys = {}

_G.empty = false

local function update_on_empty_bindings ()
  local s = awful.screen.focused()
  local k = require("keybindings")

  if #s.clients == 0 and not _G.empty then
    root.keys(gears.table.join(k.globalkeys, k.on_empty_keys))
    _G.empty = true
  elseif #s.clients > 0 and _G.empty then
    root.keys(k.globalkeys)
    _G.empty = false
  end
end

function on_empty_keys.enable ()
  tag.connect_signal("untagged", update_on_empty_bindings)
  tag.connect_signal("tagged", update_on_empty_bindings)
  screen.connect_signal("tag::history::update", update_on_empty_bindings)
end

return on_empty_keys
