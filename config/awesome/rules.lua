local beautiful = require("beautiful")
local awful = require("awful")
local naughty = require("naughty")

local keys = require("keybindings")

return {
	-- All clients will match this rule.
	{ rule = { },
		properties = { border_width = beautiful.border_width,
			border_color = beautiful.border_normal,
			focus = awful.client.focus.filter,
			raise = true,
			keys = keys.clientkeys,
			buttons = keys.clientbuttons,
			screen = awful.screen.preferred,
			placement = awful.placement.no_overlap+awful.placement.no_offscreen,
      size_hints_honor = false,
		}
	},

	-- floating windows
	{ rule_any = {
		instance = {
			"DTA",	-- Firefox addon DownThemAll.
			"copyq",	-- Includes session name in class.
			"pinentry",
		},
		class = {
			"Arandr",
			"Blueman-manager",
			"Gpick",
			"Kruler",
			"MessageWin",  -- kalarm.
			"Sxiv",
			"Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
			"Wpa_gui",
			"veromix",
			"xtightvncviewer"},

		-- Note that the name property shown in xprop might be set slightly after creation of the client
		-- and the name shown there might not match defined rules here.
		name = {
			"Event Tester",  -- xev.
		},
		role = {
			"AlarmWindow",	-- Thunderbird's calendar.
			"ConfigManager",	-- Thunderbird's about:config.
			"pop-up",				-- e.g. Google Chrome's (detached) Developer Tools.
		}
		}, properties = { floating = true, ontop = true }},

	-- Add titlebars to clients and dialogs
	{ rule_any = {type = { "dialog" }
		}, properties = { titlebars_enabled = true }
	},

	{
		rule = { class = "Emacs" },
		properties = { tag = "4", switchtotag = true, screen = 1 }
	},

	{
		rule = { class = "KeePassXC" },
		properties = { tag = "8", switchtotag = true, screen = 1 }
	},

	{
		rule_any = { class = {"discord", "spotify"} },
		properties = { screen = 2 }
	},
}
