local lib = {}

function lib.is_in_table(element, table)
  local res = false
  for _, value in pairs(table) do
    if element:match(value) then
      res = true
      break
    end
  end
  return res
end


return lib
