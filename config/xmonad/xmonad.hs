{-# OPTIONS_GHC -Wno-unrecognised-pragmas #-}
{-# HLINT ignore "Redundant return" #-}
import XMonad
import XMonad.Layout
import XMonad.Layout.Renamed
import XMonad.Layout.ToggleLayouts
import XMonad.Layout.NoBorders

import XMonad.Layout.ThreeColumns
import XMonad.Layout.TwoPane
import XMonad.Layout.TrackFloating

import XMonad.Hooks.InsertPosition

import XMonad.Util.EZConfig
import XMonad.Util.Ungrab
import qualified XMonad.StackSet as W

import XMonad.Hooks.EwmhDesktops

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.StatusBar
import XMonad.Hooks.StatusBar.PP
import XMonad.Hooks.ManageDocks
import XMonad.Util.Loggers
import XMonad.Hooks.ManageHelpers

import XMonad.Util.SpawnOnce


import XMonad.Actions.UpdatePointer

import XMonad.Util.ClickableWorkspaces

myStartupHook = do
  spawnOnce "nitrogen --restore"
  return ()
  checkKeymap myConfig myKeymap

myKeymap = [
    ("M-<Return>", spawn "xst")
  , ("M-S-<Return>", windows W.swapMaster)

    -- KOY bindings
  , ("M-.", kill)
  , ("M-t", windows W.focusDown)
  , ("M-S-t", windows W.swapDown)
  , ("M-r", windows W.focusUp)
  , ("M-S-r", windows W.swapUp)
  , ("M-d", sendMessage Shrink)
  , ("M-n", sendMessage Expand)
  , ("M-M1-i", sendMessage (IncMasterN 1))
  , ("M-M1-d", sendMessage (IncMasterN (-1)))
  , ("M-k", refresh)
  , ("<Print>", unGrab *> spawn "maim -s | xclip -selection clipboard -t image/png")
  , ("C-<Print>", spawn "maim -i $(xdotool getwindowfocus)| xclip -selection clipboard -t image/png")

  , ("<XF86AudioPlay>", spawn "playerctl -p spotify play-pause")
  , ("<XF86AudioNext>", spawn "playerctl -p spotify next")
  , ("<XF86AudioPrev>", spawn "playerctl -p spotify previous")

  , ("M-f", withFocused $ windows . W.sink)
  , ("M-x", sendMessage (Toggle "Full"))
  , ("M-p", spawn "rofi -show drun")
  , ("M-j", spawn "thunar")
  , ("M-b", sendMessage ToggleStruts)

  , ("C-M1-<Delete>", spawn "$DOTFILES_BIN/rofi/powermenu")
  ]

myManageHook :: ManageHook
myManageHook = composeAll
    [ className =? "Gimp" --> doFloat
    , className =? "kdeconnect.daemon" --> doFullFloat
    , isDialog            --> doFloat <+> doF W.swapUp
    , checkDock           --> doLower
    , className =? "Ulauncher" --> hasBorder False <+> doF W.swapUp <+> doFloat
    ]

myLayout = avoidStruts $ toggleLayouts Full (tiled ||| Mirror tiled ||| Full ||| threeCol)
  where
    threeCol = ThreeColMid nmaster delta ratio
    tiled    = Tall nmaster delta ratio
    nmaster  = 1      -- Default number of windows in the master pane
    ratio    = 1/2    -- Default proportion of screen occupied by master pane
    delta    = 3/100  -- Percent of screen to increment by when resizing panes


myConfig = def {
  modMask = mod4Mask
  , startupHook = myStartupHook
  , normalBorderColor = "#282a36"
  , focusedBorderColor = "#6272a4"
  , manageHook = insertPosition Below Newer <> myManageHook
  , layoutHook = smartBorders myLayout
  , logHook = updatePointer (0.5, 0.5) (0, 0)
} `additionalKeysP` myKeymap


myXmobarPP :: PP
myXmobarPP = def
    { ppSep             = magenta " • "
    , ppTitleSanitize   = xmobarStrip
    , ppCurrent         = wrap " " "" . xmobarBorder "Top" "#8be9fd" 2
    , ppHidden          = white . wrap " " ""
    , ppHiddenNoWindows = lowWhite . wrap " " ""
    , ppUrgent          = red . wrap (yellow "!") (yellow "!")
    , ppOrder           = \[ws, l, _, wins] -> [ws, l, wins]
    , ppExtras          = [logTitles formatFocused formatUnfocused]
    }
  where
    formatFocused   = wrap (white    "[") (white    "]") . magenta . ppWindow
    formatUnfocused = wrap (lowWhite "[") (lowWhite "]") . blue    . ppWindow

    -- | Windows should have *some* title, which should not not exceed a
    -- sane length.
    ppWindow :: String -> String
    ppWindow = xmobarRaw . (\w -> if null w then "untitled" else w) . shorten 30

    blue, lowWhite, magenta, red, white, yellow :: String -> String
    magenta  = xmobarColor "#ff79c6" ""
    blue     = xmobarColor "#bd93f9" ""
    white    = xmobarColor "#f8f8f2" ""
    yellow   = xmobarColor "#f1fa8c" ""
    red      = xmobarColor "#ff5555" ""
    lowWhite = xmobarColor "#bbbbbb" ""

mySB = statusBarProp "xmobar" (pure myXmobarPP)
main :: IO ()
main = xmonad $ ewmhFullscreen $ ewmh $ withSB mySB $ docks $ myConfig
