vim.cmd([[
noremap h d
noremap d h
noremap k r
noremap r k
noremap j t
noremap t j
noremap l n
noremap n l

noremap H D
noremap D H
noremap K R
noremap R K
noremap J T
noremap T J
noremap L N
noremap N L

noremap zp zk
noremap zn zj
]])

local ops = { noremap = true, silent = true}
map("n", "H", "d$", ops)

map("n", "l", "nzz", ops)
map("n", "L", "Nzz", ops)

map("n", "D", "^", {})
map("n", "N", "$", {})
