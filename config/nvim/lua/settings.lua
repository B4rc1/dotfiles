-- always ask to confirm if file modified
vim.o.confirm = true

--Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- enable mouse support
vim.o.mouse = "a"

vim.o.guifont = "FiraCode Nerd Font:h12"
vim.g.neovide_scroll_animation_length = 0.1

-- preview subsitutions
vim.o.inccommand = "nosplit"

-- use system clipboard as default register
vim.o.clipboard = "unnamedplus"

-- automatically write the file if switchting files
vim.o.autowrite = true

-- highlight yank
vim.cmd([[au TextYankPost * silent! lua vim.highlight.on_yank {higroup = 'IncSearch'}]])

-- permanent undo
vim.bo.undofile = true

-- tabs as 2 spaces
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.softtabstop = 2

-- mix of relative and absolute line numbers
vim.wo.number = true
vim.wo.relativenumber = true


-- use {{{  }}} as fold markers
vim.opt.foldmethod = "manual"
-- open all folds by default
-- vim.opt.foldlevel = 20

-- use "real" colors instead of terminal colors
vim.o.termguicolors = true

-- hide --INSERT-- under the status bar
vim.o.showmode = false
vim.cmd([[filetype indent off]])

-- 10% transparent completion menu
vim.opt.pumblend = 10

-- better tex support
vim.g.tex_flavor = "latex"

-- return to last position in buffer the first time opening it
vim.cmd([[
    autocmd BufRead * autocmd FileType <buffer> ++once
      \ if &ft !~# 'commit\|rebase' && line("'\"") > 1 && line("'\"") <= line("$") | exe 'normal! g`"' | endif
]])

-- make buffer content be stable (keep same lines on screen)
vim.opt.splitkeep = "screen"
