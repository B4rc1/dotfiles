function use(plug)
  table.insert(_G.plugins, plug)
end

local function bind(op, outer_opts)
    outer_opts = outer_opts or {noremap = true}
    return function(lhs, rhs, opts)
        opts = vim.tbl_extend("force",
            outer_opts,
            opts or {}
        )
        vim.keymap.set(op, lhs, rhs, opts)
    end
end

nmap = bind("n", {noremap = false})
nnoremap = bind("n")
vnoremap = bind("v")
xnoremap = bind("x")
inoremap = bind("i")


map = vim.api.nvim_set_keymap
unmap = vim.api.nvim_del_keymap

function mapleader(key, command)
	map("n", "<Leader>" .. key, command, {noremap = true, silent = true})
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end
