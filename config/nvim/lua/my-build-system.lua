local Path = require("plenary.path")

-- utils {{{
local function file_exists(path)
  return Path:new(path):exists()
end

-- WARN: from :h filename-modifiers
-- "For a file name that does not exist and does not have an absolute path the result is unpredictable."
local function get_abs_path(path)
  return vim.fn.fnamemodify(path, ":p")
end

-- }}}

nnoremap("ü", function ()
  local ft = vim.bo.filetype
  local cwd = vim.fn.getcwd()

  -- if vim.fn.fnamemodify(vim.fn.getcwd(), ":p"):find("^/etc/nixos") ~= nil then
  if get_abs_path(cwd):find("^/etc/nixos") ~= nil then
    vim.cmd [[TermExec cmd="sudo nixos-rebuild switch"]]
    return
  end

  if ft == "python" then
    if file_exists(cwd .. "/main.py") then
      vim.cmd [[TermExec cmd="python3 main.py"]]
      return
    else
      vim.cmd [[TermExec cmd="python3 %"]]
      return
    end

  end

  if ft == "rust" then
    if os.execute("nu -c 'open Cargo.toml | get -i lib | is-empty | exit ($in == true | into int)'") == 0 then
      vim.cmd [[TermExec cmd="cargo build"]]
      return
    else
      vim.cmd [[TermExec cmd="cargo run"]]
      return
    end
  end

  -- generic fallback
  vim.cmd("silent make")
  -- open quickfix
  vim.cmd("copen")
  -- but still focus the previous window since we already jump to the code that caused the error
  vim.cmd("wincmd p")
end)
