vim.o.completeopt = "menu,menuone,noselect"

return {
{ 'hrsh7th/nvim-cmp',
  version = false,
	dependencies = {
		'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-cmdline',
    'hrsh7th/cmp-path',
		'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-emoji',
    'kdheepak/cmp-latex-symbols',
    'petertriho/cmp-git',

    'L3MON4D3/LuaSnip',
    'saadparwaiz1/cmp_luasnip',
  },
	config = function ()

    vim.opt.runtimepath = vim.opt.runtimepath + SNIPPET_DIR

		-- Setup nvim-cmp.
		local cmp = require'cmp'
    local luasnip = require("luasnip")
    local types = require("luasnip.util.types")

    luasnip.config.set_config({
      history = true,
      -- link_children = true,
      -- link_roots = false

      -- Update more often, :h events for more info.
      updateevents = "TextChanged,TextChangedI",
      -- Snippets aren't automatically removed if their text is deleted.
      -- `delete_check_events` determines on which events (:h events) a check for
      -- deleted snippets is performed.
      -- This can be especially useful when `history` is enabled.
      delete_check_events = "TextChanged",
      ext_opts = {
        [types.choiceNode] = {
          active = {
            virt_text = { { "choiceNode", "Comment" } },
          },
        },
      },
      -- treesitter-hl has 100, use something higher (default is 200).
      ext_base_prio = 300,
      -- minimal increase in priority.
      ext_prio_increase = 1,
      enable_autosnippets = true,
      -- mapping for cutting selected text so it's usable as SELECT_DEDENT,
      -- SELECT_RAW or TM_SELECTED_TEXT (mapped via xmap).
      store_selection_keys = "<Tab>",
      -- luasnip uses this function to get the currently active filetype. This
      -- is the (rather uninteresting) default, but it's possible to use
      -- eg. treesitter for getting the current filetype by setting ft_func to
      -- require("luasnip.extras.filetype_functions").from_cursor (requires
      -- `nvim-treesitter/nvim-treesitter`). This allows correctly resolving
      -- the current filetype in eg. a markdown-code block or `vim.cmd()`.
      ft_func = function()
        local ft = require("luasnip.extras.filetype_functions").from_cursor_pos()
        if vim.tbl_isempty(ft) then
          ft = vim.split(vim.bo.filetype, ".", true)
        end

        -- new markdown parser differentiates between markdown_inline and markdown
        -- for now markdown_inline is too broad to do anything useful with. So
        -- just also set markdown.
        for _, v in pairs(ft) do
          if v == "markdown_inline" then
            table.insert(ft, "markdown")
          end
        end

        return ft
      end,
    })

    require("luasnip.loaders.from_lua").load({paths = SNIPPET_DIR .. "/lua/snippets"})

    vim.cmd [[command! LuaSnipEdit :lua require("luasnip.loaders.from_lua").edit_snippet_files()]]

    local kind_icons = {
      Text = "",
      Method = "ƒ",
      Function = "ƒ",
      Constructor = "ƒ",
      Field = "",
      Variable = "",
      Class = "",
      Interface = "",
      Module = "",
      Property = "",
      Unit = "",
      Value = "",
      Enum = "",
      Keyword = "",
      Snippet = "",
      Color = "",
      File = "",
      Reference = "&",
      Folder = "",
      EnumMember = "",
      Constant = "",
      Struct = "",
      Event = "",
      Operator = "",
      TypeParameter = ""
    }

		cmp.setup({
      formatting = {
        format = function(entry, vim_item)

          vim_item.kind = string.format('%s %s', kind_icons[vim_item.kind], vim_item.kind)

          vim_item.menu = ({
            path = "[Path]",
            buffer = "[Buffer]",
            nvim_lsp = "[LSP]",
            emoji = "[emoji]",
            luasnip = "[LuaSnip]",
            neorg = "[neorg]",
            latex_symbols = "[Latex]",
          })[entry.source.name]

          return vim_item
        end
      },
      snippet = {
        expand = function(args)
          require'luasnip'.lsp_expand(args.body)
        end
      },
      mapping = cmp.mapping.preset.insert {
        ['<C-r>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
        ['<C-t>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<CR>'] = cmp.mapping.confirm({ select = false }),
        ["<Tab>"] = cmp.mapping(function(fallback)
          if luasnip.expand_or_locally_jumpable() then
            luasnip.expand_or_jump()
          else
            fallback()
          end
        end, { "i", "s" }),

        ["<S-Tab>"] = cmp.mapping(function(fallback)
          if luasnip.locally_jumpable(-1) then
            luasnip.jump(-1)
          else
            fallback()
          end
        end, { "i", "s" }),
			},
			sources = {
				{ name = 'nvim_lsp' },
        { name = 'latex_symbols' },
        { name = 'path' },
				{ name = 'luasnip' },
        { name = 'emoji' },
				{ name = 'buffer' },
        { name = "neorg" }
			},
      experimental = {
        ghost_text = true,
      }
		})

    vim.api.nvim_set_keymap("i", "<C-E>", "<Plug>luasnip-next-choice", {})
    vim.api.nvim_set_keymap("s", "<C-E>", "<Plug>luasnip-next-choice", {})

    cmp.setup.filetype('gitcommit', {
      sources = cmp.config.sources({
        { name = 'git' }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
      }, {
        { name = 'buffer' },
      })
    })

    -- Use buffer source for `/`.
    cmp.setup.cmdline('/', {
      mapping = cmp.mapping.preset.cmdline(),
      view = {
        entries = { name = "wildmenu", seperator = "|" }
      },
      sources = {
        { name = 'buffer' }
      }
    })

    -- Use cmdline & path source for ':'.
    cmp.setup.cmdline(':', {
      mapping = cmp.mapping.preset.cmdline(),
      sources = cmp.config.sources({
        { name = 'path' }
      }, {
        { name = 'cmdline' }
      })
    })

	end
},
}
