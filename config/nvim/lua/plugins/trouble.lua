return {
{
  "folke/trouble.nvim",
  dependencies = { "kyazdani42/nvim-web-devicons" },
  config = function()
    require("trouble").setup {
      keys = {
        t = "next",
        r = "prev",
      },
      modes = {
        my_symbols = {
          desc = "document symbols",
          mode = "lsp_document_symbols",
          focus = false,
          win = { position = "right", size = { width = 0.3, height = 1.0 } },
          filter = {
            -- remove Package since luals uses it for control flow structures
            ["not"] = { ft = "lua", kind = "Package" },
          }
        }
      }
    }

		-- Open Trouble
		mapleader("od", "<Cmd>Trouble diagnostics<CR>")
    -- Open Symbols
		mapleader("os", "<Cmd>Trouble my_symbols<CR>")
  end
},
{
  "folke/todo-comments.nvim",
  dependencies = { "nvim-lua/plenary.nvim",  "folke/trouble.nvim" },
  config = function()
    require("todo-comments").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
    }
		-- Open Comments
		mapleader("ot", "<Cmd>TodoTrouble<CR>")
  end
},
}
