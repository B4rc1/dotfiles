return {
{
  "vhyrro/luarocks.nvim",
  priority = 1000, -- Very high priority is required, luarocks.nvim should run as the first plugin in your config.
  config = true,
},
{
    "nvim-neorg/neorg",
    dependencies = { "luarocks.nvim" },
    config = function()
        require("neorg").setup({
          load = {
            ["core.defaults"] = {},
            ["core.concealer"] = {},
            ["core.completion"] = {
              config = { engine = "nvim-cmp" },
            },
          },
        })
    end,
}
-- {
--     "nvim-neorg/neorg",
--     run = ":Neorg sync-parsers",
--     config = function()
--         require('neorg').setup {
--             -- Tell Neorg what modules to load
--             load = {
--                 ["core.defaults"] = {}, -- Load all the default modules
--                 ["core.completion"] = {
--                   config = {
--                     engine = "nvim-cmp",
--                   }
--                 },
--                 ["core.concealer"] = {
--                 },
--             },
--         }
--     end,
--     dependencies = "nvim-lua/plenary.nvim"
-- }
}
