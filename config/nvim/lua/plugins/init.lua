return {
{
  "yorumicolors/yorumi.nvim",
  -- lazy = false, -- make sure we load this during startup if it is your main colorscheme
  -- priority = 1000, -- make sure to load this before all the other start plugins
  config = function()
    -- load the colorscheme here
    vim.cmd.colorscheme("yorumi")
  end,
},

-------------------------------------
{ 'airblade/vim-rooter', config = function()
  vim.g.rooter_silent_chdir = true
  vim.g.rooter_patterns = {
    '>tagify-monorepo',
    '.git',
    'Makefile',
    '*.sln',
    'build/env.sh',
    '.zk',
    '.obsidian'
  }
  -- vim.g.rooter_change_directory_for_non_project_files = 'current'
end }, -- autocd
-------------------------------------
{
  'altermo/ultimate-autopair.nvim',
  event={ 'InsertEnter','CmdlineEnter' },
  after = 'hrsh7th/nvim-cmp',
  branch='v0.6', -- NOTE: Needs to be Updated manually
  config = function ()
    require'ultimate-autopair'.init({
      require'ultimate-autopair'.extend_default {
        -- See https://github.com/altermo/ultimate-autopair.nvim/issues/68#issuecomment-1994809932
        config_internal_pairs = {
          { "'", "'", multiline = false, surround = true,
            cond = function(fn)
              if fn.get_ft() ~= 'rust' then return true end
              return not fn.in_node({ 'bounded_type', 'reference_type', 'type_arguments', 'type_parameters' })
            end,
          },
        },
      },
     })
  end
},
-------------------------------------
{
    'numToStr/Comment.nvim',
    config = function()
        require('Comment').setup()
				local ft = require('Comment.ft')
        ft.set("nix", {'#%s', '/*%s*/'})
    end
},
-------------------------------------
{ 'NvChad/nvim-colorizer.lua',
  config = function ()
    require("colorizer").setup {
      filetypes = {
        "*",  -- highlight all by default
        css = { names = true, rgb_fn = true } -- only highlight names in css
      },
      user_default_options = {
        names = false,
        RRGGBBAA = true,
        css = true,
      }
    }
  end
}, -- colorize text like #ff0 or #ff0000
-------------------------------------
{ 'lewis6991/gitsigns.nvim',         -- Very interesting seeming git plugin. TODO: check this out
  dependencies = { 'nvim-lua/plenary.nvim' },
	config = function ()
    require('gitsigns').setup {
      on_attach = function(bufnr)
          local gs = package.loaded.gitsigns

          local function map(mode, l, r, opts)
            opts = opts or {}
            opts.buffer = bufnr
            vim.keymap.set(mode, l, r, opts)
          end

          -- Navigation
          map('n', ']c', function()
            if vim.wo.diff then return ']c' end
            vim.schedule(function() gs.next_hunk() end)
            return '<Ignore>'
          end, {expr=true})

          map('n', '[c', function()
            if vim.wo.diff then return '[c' end
            vim.schedule(function() gs.prev_hunk() end)
            return '<Ignore>'
          end, {expr=true})

          -- Actions
          map('n', '<leader>gs', gs.stage_hunk)
          map('n', '<leader>gr', gs.reset_hunk)
          map('v', '<leader>gs', function() gs.stage_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
          map('v', '<leader>gr', function() gs.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
          map('n', '<leader>gS', gs.stage_buffer)
          map('n', '<leader>gu', gs.undo_stage_hunk)
          map('n', '<leader>gR', gs.reset_buffer)
          map('n', '<leader>gp', gs.preview_hunk)
          map('n', '<leader>gb', function() gs.blame_line{full=true} end)
          map('n', '<leader>gtb', gs.toggle_current_line_blame)
          map('n', '<leader>gd', gs.diffthis)
          map('n', '<leader>gD', function() gs.diffthis('~') end)
          map('n', '<leader>gtd', gs.toggle_deleted)

          -- Text object
          map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
        end
    }
	end
},
-------------------------------------
{'plasticboy/vim-markdown',
  branch = 'master',
  require = {'godlygeek/tabular'},
  config = function ()
    vim.g.vim_markdown_math = true
  end,
},
-------------------------------------
{'mbbill/undotree', config = function ()
  -- open undo
	mapleader("ou", "<Cmd>UndotreeToggle<CR><Cmd>UndotreeFocus<CR>")
end},
-------------------------------------
{
  "folke/zen-mode.nvim",
  config = function()
    require("zen-mode").setup {
    }
    mapleader("tz", "<Cmd>ZenMode<CR>")
  end
},
-------------------------------------
{
  "folke/which-key.nvim",
  config = function()
    local presets = require("which-key.plugins.presets")
    presets.operators["d"] = nil

    require("which-key").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
      -- operators = { gc = "Comments" },
			-- triggers_blacklist = {
			-- 	i = { ",", "," },
			-- },
    }
  end
},
-------------------------------------
{'Olical/conjure', tag="v4.21.0"},
-------------------------------------
-- use { 'eraserhd/parinfer-rust', run = 'cargo build --release' }
-------------------------------------
{
	'kdheepak/lazygit.nvim',
	config = function ()
    vim.g.lazygit_floating_window_scaling_factor = 1
    vim.g.lazygit_floating_window_corner_chars = { "", "", "", "" }
    -- git git
		mapleader("gg", ":LazyGit<CR>")
	end
},
-------------------------------------
{
  "akinsho/toggleterm.nvim",
  config = function ()
    require("toggleterm").setup{
      hide_numbers = false,
      shade_terminals = false,
      start_in_insert = false,
      direction = 'horizontal',
      size = function(term)
        return vim.o.lines * 0.2
      end,
    }

    vim.keymap.set('n', '<leader>tt', '<Cmd>exe v:count1 . "ToggleTerm"<CR>')
  end
},
-------------------------------------
{
  'ptzz/lf.vim',
	dependencies = { 'voldikss/vim-floaterm' },
	config = function ()
		vim.g.lf_map_keys = false
		vim.g.lf_replace_netrw = true
		vim.g.lf_width = 0.8
		vim.g.lf_height = 0.8
		mapleader("ff", "<Cmd>Lf<CR>")
	end
},
-------------------------------------
{
	'David-Kunz/treesitter-unit',
	config = function ()
		map('x', 'iu', ':lua require"treesitter-unit".select()<CR>', {noremap=true})
		map('x', 'au', ':lua require"treesitter-unit".select(true)<CR>', {noremap=true})
		map('o', 'iu', ':<c-u>lua require"treesitter-unit".select()<CR>', {noremap=true})
		map('o', 'au', ':<c-u>lua require"treesitter-unit".select(true)<CR>', {noremap=true})
	end
},
-------------------------------------
{
	'andymass/vim-matchup',
  branch = "master",
  config = function ()
    vim.g.matchup_matchparen_offscreen = {
      ['method'] = 'popup',
      ['border'] = 1
    }
  end
},
-------------------------------------
{
  'kyazdani42/nvim-tree.lua',
  dependencies = {
    'kyazdani42/nvim-web-devicons', -- optional, for file icon
    'sindrets/diffview.nvim',
  },
  config = function()
    require'nvim-tree'.setup {
      disable_netrw       = false,
      hijack_netrw        = true,
      open_on_tab         = false,
      hijack_cursor       = false,
      update_cwd          = false,
      hijack_directories = {
        enable = true,
        auto_open = true,
      },
      actions = {
        open_file = {
          resize_window = true,
        },
      },
      diagnostics = {
        enable = true,
        icons = {
          hint = "",
          info = "",
          warning = "",
          error = "",
        }
      },
      update_focused_file = {
        enable      = true,
        update_cwd  = false,
        ignore_list = {}
      },
      system_open = {
        cmd  = "xdg-open",
        args = {}
      },
      filters = {
        dotfiles = false,
        custom = {}
      },
      git = {
        enable = true,
        ignore = false,
        timeout = 500,
      },
      view = {
        width = 30,
        side = 'left',
        number = false,
        relativenumber = false,
        signcolumn = "yes"
      },
      renderer = {
        highlight_opened_files = "icon",
        indent_markers = {
          enable = false,
          icons = {
            corner = "└ ",
            edge = "│ ",
            none = "  ",
          },
        },
      },
      trash = {
        cmd = "trash",
        require_confirm = true
      },
      on_attach = function (bufnr)
        local api = require('nvim-tree.api')

        local function opts(desc)
          return { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
        end

        vim.keymap.set('n', '<CR>', api.node.open.edit, opts('Open'))
        vim.keymap.set('n', 'o', api.node.open.edit, opts('Open'))
        vim.keymap.set('n', '<2-LeftMouse>', api.node.open.edit, opts('Open'))
        vim.keymap.set('n', 'O', api.node.open.no_window_picker, opts('Open: No Window Picker'))
        vim.keymap.set('n', '<2-RightMouse>', api.tree.change_root_to_node, opts('CD'))
        vim.keymap.set('n', 'gd', api.tree.change_root_to_node, opts('CD'))
        vim.keymap.set('n', '<C-v>', api.node.open.vertical, opts('Open: Vertical Split'))
        vim.keymap.set('n', '<C-x>', api.node.open.horizontal, opts('Open: Horizontal Split'))
        vim.keymap.set('n', '<C-t>', api.node.open.tab, opts('Open: New Tab'))
        vim.keymap.set('n', '<', api.node.navigate.sibling.prev, opts('Previous Sibling'))
        vim.keymap.set('n', '>', api.node.navigate.sibling.next, opts('Next Sibling'))
        vim.keymap.set('n', 'P', api.node.navigate.parent, opts('Parent Directory'))
        vim.keymap.set('n', '<BS>', api.node.navigate.parent_close, opts('Close Directory'))
        vim.keymap.set('n', '<Tab>', api.node.open.preview, opts('Open Preview'))
        vim.keymap.set('n', 'R', api.node.navigate.sibling.first, opts('First Sibling'))
        vim.keymap.set('n', 'T', api.node.navigate.sibling.last, opts('Last Sibling'))
        vim.keymap.set('n', 'I', api.tree.toggle_gitignore_filter, opts('Toggle Git Ignore'))
        vim.keymap.set('n', 'J', api.tree.reload, opts('Refresh'))
        vim.keymap.set('n', 'a', api.fs.create, opts('Create'))
        vim.keymap.set('n', 'h', api.fs.remove, opts('Delete'))
        vim.keymap.set('n', 'H', api.fs.trash, opts('Trash'))
        vim.keymap.set('n', 'j', api.fs.rename, opts('Rename'))
        vim.keymap.set('n', '<C-r>', api.fs.rename_sub, opts('Rename: Omit Filename'))
        vim.keymap.set('n', 'x', api.fs.cut, opts('Cut'))
        vim.keymap.set('n', 'c', api.fs.copy.node, opts('Copy'))
        vim.keymap.set('n', 'p', api.fs.paste, opts('Paste'))
        vim.keymap.set('n', 'y', api.fs.copy.filename, opts('Copy Name'))
        vim.keymap.set('n', 'Y', api.fs.copy.relative_path, opts('Copy Relative Path'))
        vim.keymap.set('n', 'gy', api.fs.copy.absolute_path, opts('Copy Absolute Path'))
        vim.keymap.set('n', '[c', api.node.navigate.git.prev, opts('Prev Git'))
        vim.keymap.set('n', ']c', api.node.navigate.git.next, opts('Next Git'))
        vim.keymap.set('n', '-', api.tree.change_root_to_parent, opts('Up'))
        vim.keymap.set('n', 's', api.node.run.system, opts('Run System'))
        vim.keymap.set('n', 'q', api.tree.close, opts('Close'))
        vim.keymap.set('n', 'g?', api.tree.toggle_help, opts('Help'))
      end
    }
    mapleader("of", "<Cmd>NvimTreeToggle<CR>")
  end
},
-------------------------------------
{
  'sindrets/diffview.nvim',
  dependencies = {
    'nvim-lua/plenary.nvim',
    'kyazdani42/nvim-web-devicons', -- optional, for file icon
  },
  config = function ()
    local cb = require'diffview.config'.diffview_callback

    require'diffview'.setup {
      key_bindings = {
        disable_defaults = false,                   -- Disable the default key bindings
        -- The `view` bindings are active in the diff buffers, only when the current
        -- tabpage is a Diffview.
        view = {
          ["<tab>"]      = cb("select_next_entry"),  -- Open the diff for the next file
          ["<s-tab>"]    = cb("select_prev_entry"),  -- Open the diff for the previous file
          ["gf"]         = cb("goto_file"),          -- Open the file in a new split in previous tabpage
          ["gs"]         = cb("goto_file_split"),    -- Open the file in a new split
          ["<C-w>gf"]    = cb("goto_file_tab"),      -- Open the file in a new tabpage
          ["<leader>e"]  = cb("focus_files"),        -- Bring focus to the files panel
          ["<leader>b"]  = cb("toggle_files"),       -- Toggle the files panel.
          ["q"]          = cb("close"),
        },
        file_panel = {
          ["t"]             = cb("next_entry"),           -- Bring the cursor to the next file entry
          ["<down>"]        = cb("next_entry"),
          ["r"]             = cb("prev_entry"),           -- Bring the cursor to the previous file entry.
          ["<up>"]          = cb("prev_entry"),
          ["<cr>"]          = cb("select_entry"),         -- Open the diff for the selected entry.
          ["o"]             = cb("select_entry"),
          ["<2-LeftMouse>"] = cb("select_entry"),
          ["-"]             = cb("toggle_stage_entry"),   -- Stage / unstage the selected entry.
          ["S"]             = cb("stage_all"),            -- Stage all entries.
          ["U"]             = cb("unstage_all"),          -- Unstage all entries.
          ["X"]             = cb("restore_entry"),        -- Restore entry to the state on the left side.
          ["R"]             = cb("refresh_files"),        -- Update stats and entries in the file list.
          ["<tab>"]         = cb("select_next_entry"),
          ["<s-tab>"]       = cb("select_prev_entry"),
          ["gf"]            = cb("goto_file"),
          ["gs"]            = cb("goto_file_split"),
          ["<C-w>gf"]       = cb("goto_file_tab"),
          ["i"]             = cb("listing_style"),        -- Toggle between 'list' and 'tree' views
          ["f"]             = cb("toggle_flatten_dirs"),  -- Flatten empty subdirectories in tree listing style.
          ["<leader>e"]     = cb("focus_files"),
          ["<leader>b"]     = cb("toggle_files"),
          ["q"]             = cb("close"),
        },
        file_history_panel = {
          ["g!"]            = cb("options"),            -- Open the option panel
          ["gd"]            = cb("open_in_diffview"),   -- Open the entry under the cursor in a diffview
          ["y"]             = cb("copy_hash"),          -- Copy the commit hash of the entry under the cursor
          ["zR"]            = cb("open_all_folds"),
          ["zM"]            = cb("close_all_folds"),
          ["t"]             = cb("next_entry"),
          ["<down>"]        = cb("next_entry"),
          ["r"]             = cb("prev_entry"),
          ["<up>"]          = cb("prev_entry"),
          ["<cr>"]          = cb("select_entry"),
          ["o"]             = cb("select_entry"),
          ["<2-LeftMouse>"] = cb("select_entry"),
          ["<tab>"]         = cb("select_next_entry"),
          ["<s-tab>"]       = cb("select_prev_entry"),
          ["gf"]            = cb("goto_file"),
          ["gs"]    = cb("goto_file_split"),
          ["<C-w>gf"]       = cb("goto_file_tab"),
          ["<leader>e"]     = cb("focus_files"),
          ["<leader>b"]     = cb("toggle_files"),
          ["q"]             = cb("close"),
        },
        option_panel = {
          ["<tab>"] = cb("select"),
          ["q"]     = cb("close"),
        },
      },
    }
  end
},
-------------------------------------
{
  'jubnzv/mdeval.nvim',
  config = function ()
    require 'mdeval'.setup({
      -- Don't ask before executing code blocks
      require_confirmation=false,
      -- Change code blocks evaluation options.
      exec_timeout = 15,
      eval_options = {
        -- Set custom configuration for C++
        cpp = {
          command = {"g++", "-O0"},
          default_header = [[
#include <iostream>
#include <vector>
          using namespace std;
          ]]
        },
      },
    })

    vim.api.nvim_set_keymap('n', '<C-C><C-C>',
                            "<cmd>lua require 'mdeval'.eval_code_block()<CR>",
                            {silent = true, noremap = true})
  end
},
-------------------------------------
{
  'stevearc/dressing.nvim',
  config = function ()
  end
},
-------------------------------------
{
  'junegunn/vim-easy-align',
  config = function ()
    vim.cmd[[
      xmap ga <Plug>(EasyAlign)
      nmap ga <Plug>(EasyAlign)
    ]]
  end
},
-------------------------------------
{
  'rhysd/conflict-marker.vim',
  config = function ()
    vim.g.conflict_marker_highlight_group = ''

    vim.g.conflict_marker_begin = '^<<<<<<< .*$'
    vim.g.conflict_marker_end   = '^>>>>>>> .*$'

    vim.cmd [[
      highlight ConflictMarkerBegin guibg=#2f7366
      highlight ConflictMarkerOurs guibg=#2e5049
      highlight ConflictMarkerTheirs guibg=#344f69
      highlight ConflictMarkerEnd guibg=#2f628e
      highlight ConflictMarkerCommonAncestorsHunk guibg=#754a81
    ]]
  end
},
-------------------------------------
-- {
--   'B4rc1/md-bullets.nvim',
--   config = function ()
--   end
-- },
-------------------------------------
{
  'tiagovla/scope.nvim',
  -- Buffers belong to tabs
  config = function ()
    require("scope").setup()
  end
},
-------------------------------------
{
  'sbdchd/neoformat',
},
-------------------------------------
{
  'NMAC427/guess-indent.nvim',
  config = function ()
    require('guess-indent').setup {}
  end
},
-------------------------------------
{
  'lewis6991/cleanfold.nvim',
  config = function ()
    require('cleanfold').setup()
  end
},
-------------------------------------
{
  'max397574/better-escape.nvim',
  cond = function()
    if os.getenv("__USES_KOY") then
      return false
    else
      return true
    end
  end,
  config = function()
    require("better_escape").setup {
      mapping = { "jk" }
    }
  end,
},
-------------------------------------
-- TODO: Move to ft file especially for file type plugins
{ 'viniciusgerevini/clyde.vim' },

-------------------------------------
{ 'kevinhwang91/nvim-ufo',
	dependencies = 'kevinhwang91/promise-async',
  config = function ()
    vim.o.foldcolumn = '0' -- '0' is not bad
    vim.o.foldlevel = 99 -- Using ufo provider need a large value
    vim.o.foldlevelstart = 99
    vim.o.foldenable = true

    require('ufo').setup()

    -- Using ufo provider need remap `zR` and `zM`
    vim.keymap.set('n', 'zR', require('ufo').openAllFolds)
    vim.keymap.set('n', 'zM', require('ufo').closeAllFolds)

    vim.keymap.set('n', 'zr', require('ufo').openFoldsExceptKinds)
    vim.keymap.set('n', 'zm', require('ufo').closeFoldsWith)

    vim.keymap.set('n', 'R', function()
      local winid = require('ufo').peekFoldedLinesUnderCursor()
      if not winid then
        vim.lsp.buf.hover()
      end
    end)

  end
},

{
  'akinsho/bufferline.nvim',
  dependencies = {
    { 'echasnovski/mini.bufremove', version = '*' },
    { 'nvim-tree/nvim-web-devicons' }
  },
  version = "*",
  config = function ()
    require("bufferline").setup {
      options = {
        close_command = function(n) require("mini.bufremove").delete(n, false) end,
        right_mouse_command = function(n) require("mini.bufremove").delete(n, false) end,
        always_show_bufferline = false,
      }
    }
    require('mini.bufremove').setup { silent = true }
    vim.keymap.set("n", '[b', '<cmd>BufferLineCyclePrev<cr>', { silent = true })
    vim.keymap.set("n", ']b', '<cmd>BufferLineCycleNext<cr>', { silent = true })
    vim.keymap.set("n", '<leader>bh', function ()
      local bd = require("mini.bufremove").delete
      if vim.bo.modified then
        local choice = vim.fn.confirm(("Save changes to %q?"):format(vim.fn.bufname()), "&Yes\n&No\n&Cancel")
        if choice == 1 then -- Yes
          vim.cmd.write()
          bd(0)
        elseif choice == 2 then -- No
          bd(0, true)
        end
      else
        bd(0)
      end
    end)
  end
},
{
  'MagicDuck/grug-far.nvim',
  config = function()
    require('grug-far').setup({
    });
  end
},
{
  'rcarriga/nvim-notify',
  config = function ()
    vim.notify = require("notify")
  end
}
}
