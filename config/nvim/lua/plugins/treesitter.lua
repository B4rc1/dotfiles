local highlight = {
    "RainbowRed",
    "RainbowYellow",
    "RainbowBlue",
    "RainbowOrange",
    "RainbowGreen",
    "RainbowViolet",
    "RainbowCyan",
}

return {
{
    "ThePrimeagen/refactoring.nvim",
    dependencies = {
        "nvim-lua/plenary.nvim",
        "nvim-treesitter/nvim-treesitter"
    },
    config = function ()
      require('refactoring').setup({})

      local opts = { noremap = true, silent = true, expr = false }
      -- Remaps for each of the four debug operations currently offered by the plugin
      vim.api.nvim_set_keymap("v", "<Leader>re", [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Function')<CR>]], opts)
      vim.api.nvim_set_keymap("v", "<Leader>rf", [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Function To File')<CR>]], opts)
      vim.api.nvim_set_keymap("v", "<Leader>rv", [[ <Esc><Cmd>lua require('refactoring').refactor('Extract Variable')<CR>]], opts)
      vim.api.nvim_set_keymap("v", "<Leader>ri", [[ <Esc><Cmd>lua require('refactoring').refactor('Inline Variable')<CR>]], opts)

      vim.api.nvim_set_keymap("n", "<leader>rpf",
          ":lua require('refactoring').debug.printf({below = false})<CR>",
          { noremap = true }
          )

      -- Print var: this remap should be made in visual mode
      vim.api.nvim_set_keymap("v", "<leader>rpv", ":lua require('refactoring').debug.print_var({})<CR>", { noremap = true })

      -- Cleanup function: this remap should be made in normal mode
      vim.api.nvim_set_keymap("n", "<leader>rpc", ":lua require('refactoring').debug.cleanup({})<CR>", { noremap = true })
    end
},

{
  "danymat/neogen",
  dependencies = "nvim-treesitter/nvim-treesitter",
  config = function()
    require('neogen').setup {
      enabled = true,
      languages = {
        lua = {
          template = {
            annotation_convention = "emmylua"
          }
        }
      }
    }

    local opts = { noremap = true, silent = true }
    -- <leader> *r*efactor *d*iagnostics *c*urrent
    vim.api.nvim_set_keymap("n", "<Leader>rdd", [[ :lua require('neogen').generate({type = "func"})<CR>]], opts)
    vim.api.nvim_set_keymap("n", "<Leader>rdf", [[ :lua require('neogen').generate({type = "file"})<CR>]], opts)
    vim.api.nvim_set_keymap("n", "<Leader>rdc", [[ :lua require('neogen').generate({type = "class"})<CR>]], opts)
    vim.api.nvim_set_keymap("n", "<Leader>rdt", [[ :lua require('neogen').generate({type = "type"})<CR>]], opts)
  end,
},

{
  'nvim-treesitter/nvim-treesitter',
  branch = "master",
  dependencies = {'nvim-treesitter/playground', 'yioneko/nvim-yati', "nushell/tree-sitter-nu",},
  config = function()
    local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

    parser_configs.norg_meta = {
        install_info = {
            url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
            files = { "src/parser.c" },
            branch = "main"
        },
    }

    parser_configs.norg_table = {
        install_info = {
            url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
            files = { "src/parser.c" },
            branch = "main"
        },
    }
    -- parser_configs.nu = {
    --   install_info = {
    --     url = "https://github.com/nushell/tree-sitter-nu",
    --     files = { "src/parser.c" },
    --     branch = "main",
    --   },
    --   filetype = "nu",
    -- }

    -- recognize my homie wgsl
    vim.api.nvim_create_autocmd({ "BufNewFile", "BufRead" }, {
      pattern = "*.wgsl",
      callback = function()
        vim.bo.filetype = "wgsl"
        local ft = require('Comment.ft')
        ft.wgsl = { '//%s' }
      end,
    })

    require'nvim-treesitter.configs'.setup {
      yati = { enable = true, default_lazy = true },
      ensure_installed = "all", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
      highlight = {
        enable = true,

        -- bad performance as of now
        disable = { "zig" },
        additional_vim_regex_highlighting = {
          'markdown',
        }
      },
      textobjects = { enable = true },
      indent = { enable = false },
      matchup = { enable = true },
    }
  end
}, -- Better Syntax highlighting with nvim 0.5

{
  'nvim-treesitter/nvim-treesitter-context',
  branch = "master";
  dependencies = {  'nvim-treesitter/nvim-treesitter' },
  config = function ()
    require'treesitter-context'.setup()
    vim.cmd([[hi TreesitterContext guibg=#01111F]])
    -- vim.keymap.set("n", "[c", function()
    --   require("treesitter-context").go_to_context()
    -- end, { silent = true })
  end
},

{ 'lukas-reineke/indent-blankline.nvim',
  main = "ibl",
  config = function ()
    require("ibl").setup({
      indent = {
        char = "│",
        tab_char = "│",
      },
      scope = {
        show_start = false,
        show_end = false,
      }
    })

    vim.cmd[[highlight! link IblScope lualine_b_normal]]
  end
}, -- indent guidelines, depends (with my config) on treesitter

{
    "sustech-data/wildfire.nvim",
    event = "VeryLazy",
    dependencies = { "nvim-treesitter/nvim-treesitter" },
    config = function()
        require("wildfire").setup()
    end,
},

{
  'HiPhish/rainbow-delimiters.nvim',
  config = function ()
    vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
    vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
    vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
    vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
    vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
    vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
    vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })

    -- This module contains a number of default definitions
    local rainbow_delimiters = require 'rainbow-delimiters'

    vim.g.rainbow_delimiters = {
      strategy = {
        [''] = rainbow_delimiters.strategy['global'],
        vim = rainbow_delimiters.strategy['local'],
      },
      query = {
        [''] = 'rainbow-delimiters',
        lua = 'rainbow-blocks',
      },
      highlight = highlight
    }
  end,
}
}
