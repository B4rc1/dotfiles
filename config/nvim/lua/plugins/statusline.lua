return {
	'nvim-lualine/lualine.nvim',
  dependencies = { 'kyazdani42/nvim-web-devicons' },
  config = function()
    require('lualine').setup{
      options = {
				theme = 'auto',
        section_separators = '',
        component_separators = '',
        icons_enabled = true,
			},

			sections = {
				lualine_a = {'mode'},
				lualine_b = {'branch'},
				lualine_c = { { 'filename', path = 1 } }, -- relative path
				lualine_x = {'encoding', 'fileformat', 'filetype', 'diagnostics'},
				lualine_y = {'progress'},
				lualine_z = {'location'}
			},

			inactive_sections = {
				lualine_a = {},
				lualine_b = {},
				lualine_c = {{ 'filename', path = 1 }},
				lualine_x = {'location'},
				lualine_y = {},
				lualine_z = {}
			},

			extensions = {}
    }
  end
}
