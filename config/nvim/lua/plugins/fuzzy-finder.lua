return {
{
    'ibhagwan/fzf-lua',
    dependencies = {
      'kyazdani42/nvim-web-devicons'
    },
    config = function()

    -- local actions = require "fzf-lua.actions"
    require'fzf-lua'.setup {
      winopts = {
        -- fullscreen = true,
        -- border = 'none',
        preview = {
          vertical = 'down:45%',
          flip_columns = 120,
        },
      },
      lsp = { lsp_icons = true },
      keymap = {
        builtin = {
          ["<C-e>"] = "preview-down",
          ["<C-y>"] = "preview-up",
          ["<C-d>"] = "preview-page-down",
          ["<C-u>"] = "preview-page-up",
        },
        fzf = {
          -- for fzf provided previews, e.g. ripgrep
          ["ctrl-e"] = "preview-down",
          ["ctrl-y"] = "preview-up",
          ["ctrl-d"] = "preview-page-down",
          ["ctrl-u"] = "preview-page-up",

          -- same as ctrl + backspace, cuz terminal reasons...
          ["ctrl-h"] = "unix-word-rubout",
          ["ctrl-l"] = "unix-line-discard",
        },
      }
    }


    map("n", "z=", "<Cmd>:lua require('fzf-lua').spell_suggest()<CR>", { noremap = true, silent = true })
    map("n", "gr", "<Cmd>:lua require('fzf-lua').lsp_references()<CR>", { noremap = true, silent = true })

    mapleader("<space>", "<Cmd>lua require('fzf-lua').files({winopts = { fullscreen = true, border = 'none' }})<CR>")
    -- mapleader("?", "<Cmd>:Telescope keymaps<CR>")
    mapleader("hh", "<Cmd>lua require('fzf-lua').help_tags()<CR>")
    mapleader("hk", "<Cmd>lua require('fzf-lua').keymaps()<CR>")
    mapleader("bb", "<Cmd>lua require('fzf-lua').buffers()<CR>")
    mapleader("fg", "<Cmd>lua require('fzf-lua').live_grep_glob({winopts = { fullscreen = true, border = 'none', preview = { layout = 'vertical' } }})<CR>")
    mapleader("ws", "<CMD>lua require('fzf-lua').lsp_live_workspace_symbols()<CR>")
    mapleader("fos", "<Cmd>lua require('fzf-lua').files({ cwd = _G.SNIPPET_DIR })<CR>")
    mapleader("om", "<Cmd>lua require('fzf-lua').marks()<CR>")
    mapleader("or", "<Cmd>lua require('fzf-lua').registers()<CR>")
    mapleader("o:", "<Cmd>lua require('fzf-lua').command_history()<CR>")

    mapleader("fc", "<Cmd>lua require('fzf-lua').git_bcommits()<CR>")

    require("fzf-lua").register_ui_select()

    end
},

{
  'junegunn/fzf',
  build = function()
    vim.fn['fzf#install']()
  end
},

{
  'kevinhwang91/nvim-bqf',
  ft = 'qf',
  config = function ()
    require('bqf').setup({
      auto_enable = true,
      auto_resize_height = true, -- highly recommended enable
      preview = {
        win_height = 12,
        win_vheight = 12,
        delay_syntax = 80,
        border_chars = {'┃', '┃', '━', '━', '┏', '┓', '┗', '┛', '█'},
        should_preview_cb = function(bufnr, qwinid)
          local ret = true
          local bufname = vim.api.nvim_buf_get_name(bufnr)
          local fsize = vim.fn.getfsize(bufname)
          if fsize > 100 * 1024 then
              -- skip file size greater than 100k
              ret = false
          elseif bufname:match('^fugitive://') then
              -- skip fugitive buffer
              ret = false
          end
          return ret
        end
      },
      func_map = {
        tab = "j",
        tabb = "J"
      },
      filter = {
        fzf = {
          extra_opts = {'--bind', 'alt-a:toggle-all', '--delimiter', '|'}
        }
      }
    })

    local fn = vim.fn

    function _G.qftf(info)
        local items
        local ret = {}
        if info.quickfix == 1 then
            items = fn.getqflist({id = info.id, items = 0}).items
        else
            items = fn.getloclist(info.winid, {id = info.id, items = 0}).items
        end
        local limit = 31
        local fname_fmt1, fname_fmt2 = '%-' .. limit .. 's', '…%.' .. (limit - 1) .. 's'
        local valid_fmt = '%s │%5d:%-3d│%s %s'
        for i = info.start_idx, info.end_idx do
            local e = items[i]
            local fname = ''
            local str
            if e.valid == 1 then
                if e.bufnr > 0 then
                    fname = fn.bufname(e.bufnr)
                    if fname == '' then
                        fname = '[No Name]'
                    else
                        fname = fname:gsub('^' .. vim.env.HOME, '~')
                    end
                    -- char in fname may occur more than 1 width, ignore this issue in order to keep performance
                    if #fname <= limit then
                        fname = fname_fmt1:format(fname)
                    else
                        fname = fname_fmt2:format(fname:sub(1 - limit))
                    end
                end
                local lnum = e.lnum > 99999 and -1 or e.lnum
                local col = e.col > 999 and -1 or e.col
                local qtype = e.type == '' and '' or ' ' .. e.type:sub(1, 1):upper()
                str = valid_fmt:format(fname, lnum, col, qtype, e.text)
            else
                str = e.text
            end
            table.insert(ret, str)
        end
        return ret
    end

    vim.o.qftf = '{info -> v:lua._G.qftf(info)}'
  end
},
}
