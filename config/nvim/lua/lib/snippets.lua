-- export a bunch of variables relevant for snippet creation
---@diagnostic disable: lowercase-global
ls = require("luasnip")

s = ls.snippet
sn = ls.snippet_node
isn = ls.indent_snippet_node
t = ls.text_node
i = ls.insert_node
f = ls.function_node
c = ls.choice_node
d = ls.dynamic_node
r = ls.restore_node
l = require("luasnip.extras").lambda
rep = require("luasnip.extras").rep
ai = require("luasnip.nodes.absolute_indexer")
p = require("luasnip.extras").partial
m = require("luasnip.extras").match
n = require("luasnip.extras").nonempty
dl = require("luasnip.extras").dynamic_lambda
fmt = require("luasnip.extras.fmt").fmt
fmta = require("luasnip.extras.fmt").fmta
types = require("luasnip.util.types")
events = require("luasnip.util.events")
conds = require("luasnip.extras.expand_conditions")
parse = ls.parser.parse_snippet
