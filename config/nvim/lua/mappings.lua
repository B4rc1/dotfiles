if os.getenv("__USES_KOY") then
    require("koy")
end
-- remove mappings I have never used and conflict with my preferences
-- unmap("i", "<C-j>")
-- unmap("i", "<C-k>")

--Remap space as leader key
vim.api.nvim_set_keymap('', '<Space>', '<Nop>', { noremap = true, silent=true})
vim.g.mapleader = " "
vim.g.maplocalleader = " m"

-- smart tab
local function t(str)
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

function _G.smart_tab()
    return vim.fn.pumvisible() == 1 and t'<C-n>' or t'<Tab>'
end

map('i', '<Tab>', 'v:lua.smart_tab()', {expr = true, noremap = true, silent = true })
vim.o.shortmess = vim.o.shortmess .. "c"

-- quit background
mapleader("qb", "<Cmd>:suspend<CR>")

-- quit quit
mapleader("qq", "<Cmd>:qall<CR>")

-- file save
mapleader("fs", "<Cmd>:w<CR>")
-- windowing
mapleader(".d", "<C-w>h")
mapleader(".t", "<C-w>j")
mapleader(".r", "<C-w>k")
mapleader(".n", "<C-w>l")
mapleader(".D", "<C-w>H")
mapleader(".T", "<C-w>J")
mapleader(".R", "<C-w>K")
mapleader(".N", "<C-w>L")
mapleader(".s", "<C-w>s")
mapleader(".v", "<C-w>v")
mapleader(".=", "<C-w>=")
mapleader(".+", "<C-w>+")
mapleader(".-", "<C-w>-")
mapleader(".h", "<Cmd>:close<CR>")

-- window delete
mapleader(".h", "<Cmd>:close<CR>")

-- buffer next
mapleader("bn", "<Cmd>:bn<CR>")
-- buffer previous
mapleader("bp", "<Cmd>:bp<CR>")


-- quickfix close
mapleader("qc", "<CMD>:cclose<CR>")
-- quickfix open
mapleader("qo", "<CMD>:copen<CR>")

-- quickfix next
mapleader("qn", "<CMD>:cprev<CR>")
-- quickfix previous
mapleader("qp", "<CMD>:cnext<CR>")

-- locationlist open
mapleader("lo", "<CMD>:lopen<CR>")
-- locationlist close
mapleader("lc", "<CMD>:lclose<CR>")
-- locationlist cycle
mapleader("ll", "<CMD>:ll<CR>")

-- Move selected line / block of text in visual mode
map("x", "R", ":move '<-2<CR>gv-gv", { noremap = true, silent = true })
map("x", "T", ":move '>+1<CR>gv-gv", { noremap = true, silent = true })

-- better normal mode
local ops = { noremap = true, silent = true}
map("n", "gH", "H", ops)
map("n", "gM", "M", ops)
map("n", "gL", "L", ops)

-- i like centering the cursor on movement so I dont lose it
map("n", "{", "{zz", ops)
map("n", "}", "}zz", ops)
map("n", "[c", "[czz", ops)
map("n", "]c", "]czz", ops)
map("n", "[s", "[szz", ops)
map("n", "]s", "]szz", ops)

-- play q macro with Q
map("n", "Q", "@q", ops)
-- use visual mode to apply macros per line
map("v", "Q", ":'<,'>:normal @q<CR>", ops)

--clone paragraph
map("n", "cp", "yap<S-}>p", ops)

-- better insert mode
-- Mapping Ctrl-Backspace does not work in terminal Vim. Following is a workaround
-- source: https://vim.fandom.com/wiki/Map_Ctrl-Backspace_to_delete_previous_word
vim.cmd([[
	noremap! <C-BS> <C-w>
	noremap! <C-h> <C-w>
]])

-- spell correct mapping
map("i", "<C-l>", "<C-g>u<Esc>[s1z=`]a<C-g>u", ops)

-- better visual mode
-- don't lose selection if indenting
map("v", ">", ">gv", {})
map("v", "<", "<gv", {})

-- terminal mappings
map("t", "<C-q>", "<C-\\><C-n>", { noremap = true })

-- replace visual selection with last deleted word
map("x", "<leader>p", [["_dP]], ops)

-- substitude all occurences of word in buffer
map("n", "<leader>s", ":%s/\\<<C-r><C-w>\\>/<C-r><C-w>/gI<Left><Left><Left>", ops)
