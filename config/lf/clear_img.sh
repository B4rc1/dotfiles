#!/usr/bin/env sh

if [ "$TERM" = "xterm-kitty" ]; then
  ${XDG_CONFIG_HOME:-~/.config}/lf/kitty/clear_img.sh "$@"
else
  ${XDG_CONFIG_HOME:-~/.config}/lf/ueberzug/clear_img.sh "$@"
fi
