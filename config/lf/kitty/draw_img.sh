#!/usr/bin/env sh

file=$1
w=$2
h=$3
x=$4
y=$5

kitty +icat --silent --transfer-mode file --place "${w}x${h}@${x}x${y}" "$file"
