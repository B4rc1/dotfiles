#!/bin/sh
file="$1"
shift

case "$(file -Lb --mime-type -- "$file")" in
  application/json)
    jq -C "$file" | bat --color always --style=numbers --paging=always
    ;;
  text/*|application/javascript)
    # highlight -O xterm256 --style=pablo "$file"
    bat "$file" --color always --style=numbers --paging=always
    ;;
  image/*)
    sxiv "$file"
    ;;
  application/pdf)
    zathura "$file"
    ;;
  video/*)
    mpv "$file"
    ;;
    # archives
    application/zip|application/rar|application/7z-compressed|\
      application/xz|application/bzip2|application/gzip|application/tar)
          atool --list -- "$file" | bat --color always --style=numbers --paging=always
          ;;
        *)
          notify-send "No preview handler for $(file -Lb --mime-type -- "$file")"
          ;;
esac
exit 0
