#!/bin/sh
draw() {
  if [ "$TERM" = "xterm-kitty" ]; then
    ${XDG_CONFIG_HOME:-~/.config}/lf/kitty/draw_img.sh "$@"
  else
    ${XDG_CONFIG_HOME:-~/.config}/lf/ueberzug/draw_img.sh "$@"
  fi
  exit 1
}

hash() {
  printf '%s/.cache/lf/%s' "$HOME" \
    "$(stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | awk '{print $1}')"
  }

cache() {
  if [ -f "$1" ]; then
    draw "$@"
  fi
}

file="$1"
shift

case "$(file -Lb --mime-type -- "$file")" in
  application/json)
    jq -C '.' "$file" | bat --color always --style=plain
    ;;
  text/*|application/javascript)
    # highlight -O xterm256 --style=pablo "$file"
    bat "$file" --color always --style=plain
    ;;
  image/*)
    if [ -n "$FIFO_UEBERZUG" ]; then
      orientation="$(identify -format '%[EXIF:Orientation]\n' -- "$file")"
      if [ -n "$orientation" ] && [ "$orientation" != 1 ]; then
        cache="$(hash "$file").jpg"
        cache "$cache" "$@"
        convert -- "$file" -auto-orient "$cache"
        draw "$cache" "$@"
      else
        draw "$file" "$@"
      fi
    fi
    ;;
  application/pdf)
    if [ -n "$FIFO_UEBERZUG" ]; then
      # cache="$(hash "$file")"
      # cache "$cache" "$@"
      # pdftoppm -f 1 -l 1 -scale-to-x 1920 -scale-to-y -1 -singlefile -jpeg -tiffcompression jpeg -- "${file}" "${cache}"
      # # pdftoppm appends the jpg extension automatically
      # draw "${cache}.jpg" "$@"

      # ghostscript is way faster
      cache="$(hash "$file").png"
      cache "$cache" "$@"
      gs -o "$cache" -sDEVICE=pngalpha -dLastPage=1 "$file" >/dev/null
      draw "$cache" "$@"
    fi
    ;;
  video/*)
    if [ -n "$FIFO_UEBERZUG" ]; then
      cache="$(hash "$file").jpg"
      cache "$cache" "$@"
      ffmpegthumbnailer -i "$file" -o "$cache" -s 0
      draw "$cache" "$@"
    fi
    ;;
    # archives
    application/zip|application/rar|application/7z-compressed|\
      application/xz|application/bzip2|application/gzip|application/tar)
          atool --list -- "$file"
          ;;
        *)
          echo '----- File Type Classification -----' && file --dereference --brief -- "${file}"
          ;;
esac
exit 0
