{ inputs, config, lib, pkgs, ... }:

with lib;
with lib.my;
{
   imports =
    # I use home-manager to deploy files to $HOME; little else
    [ inputs.home-manager.nixosModules.home-manager ]
    # All my personal modules
    ++ (mapModulesRec' (toString ./modules) import);

  # Common config for all nixos machines; and to ensure the flake operates
  # soundly
  environment.variables.DOTFILES = config.dotfiles.dir;
  environment.variables.DOTFILES_BIN = config.dotfiles.binDir;

  # Configure nix and nixpkgs
  environment.variables.NIXPKGS_ALLOW_UNFREE = "1";
  nix =
    let filteredInputs = filterAttrs (n: _: n != "self") inputs;
        nixPathInputs  = mapAttrsToList (n: v: "${n}=${v}") filteredInputs;
        registryInputs = mapAttrs (_: v: { flake = v; }) filteredInputs;
    in {
      package = pkgs.nixVersions.stable;
      extraOptions = "experimental-features = nix-command flakes";
      nixPath = nixPathInputs ++ [
        "nixpkgs-overlays=${config.dotfiles.dir}/overlays"
        "dotfiles=${config.dotfiles.dir}"
      ];
      registry = registryInputs // { dotfiles.flake = inputs.self; };
      settings = {
        substituters = [
          "https://nix-community.cachix.org"
        ];
        trusted-public-keys = [
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        ];
        auto-optimise-store = true;
        # 128MB
        min-free = 128000000;
      };
    };
  system.configurationRevision = with inputs; mkIf (self ? rev) self.rev;

 system.activationScripts.diff = mkIf (!config.programs.nh.enable) {
    supportsDryActivation = true;
    text = ''
      if [[ -e /run/current-system ]]; then
        echo "--- diff to current-system"
        ${pkgs.nvd}/bin/nvd --nix-bin-dir=${config.nix.package}/bin --color always diff /run/current-system "$systemConfig"
        echo "---"
      fi
    '';
  };

  ## Some reasonable, global defaults
  # This is here to appease 'nix flake check' for generic hosts with no
  # hardware-configuration.nix or fileSystem config.
  fileSystems."/".device = mkDefault "/dev/disk/by-label/nixos";

  # Use the latest kernel
  boot = {
    kernelPackages = mkDefault pkgs.linuxPackages_xanmod;
    loader = {
      efi.canTouchEfiVariables = mkDefault true;
      systemd-boot.configurationLimit = 10;
      systemd-boot.enable = mkDefault true;
    };
  };

  # replace sudo by doas
  # security = {
  #   sudo.enable = false;
  #   doas = {
  #     enable = true;
  #     extraConfig = ''
  #       permit nopass setenv { HOME="/home/jonas" } jonas
  #     '';
  #   };
  # };

  security.sudo.wheelNeedsPassword = false;

  # Just the bear necessities...
  environment.systemPackages = with pkgs; [
    man-pages man-pages-posix
    bind
    cached-nix-shell
    coreutils
    git
    wget
    unzip
    tree
    fd ripgrep bat
    jq
    gotop
    zip
    hyx
    nvd

    inxi
    glxinfo

    gnumake
    psmisc
    usbutils

    efibootmgr

    # misc scripting
    unstable.nushell
  ];

  environment.variables = {
    PAGER = "bat --color=always -p";
    MANPAGER="sh -c 'col -bx | bat -l man -p'";
  };


  console = {
    font = "Lat2-Terminus16";
    keyMap = "de-latin1-nodeadkeys";
  };
}
