{ pkgs ? import <nixpkgs> {} }:

with pkgs;
let 
    nixBin =
      writeShellScriptBin "nix" ''
        /run/current-system/sw/bin/nix --option experimental-features "nix-command flakes" "$@"
      '';
    nvimBin =
      writeShellScriptBin "nvim" ''
        ${pkgs.neovim}/bin/nvim -u "$FLAKE/config/nvim/init.lua"
      '';
in mkShell {
  buildInputs = [
    git
    nix-zsh-completions
  ];
  shellHook = ''
    export FLAKE="$(pwd)"
    export PATH="$FLAKE/bin:${nvimBin}/bin:$PATH"
  '';
}
