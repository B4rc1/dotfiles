"
"  ██          ██   ██               ██
" ░░          ░░   ░██              ░░
"  ██ ███████  ██ ██████    ██    ██ ██ ██████████
" ░██░░██░░░██░██░░░██░    ░██   ░██░██░░██░░██░░██
" ░██ ░██  ░██░██  ░██     ░░██ ░██ ░██ ░██ ░██ ░██
" ░██ ░██  ░██░██  ░██   ██ ░░████  ░██ ░██ ░██ ░██
" ░██ ███  ░██░██  ░░██ ░██  ░░██   ░██ ███ ░██ ░██
" ░░ ░░░   ░░ ░░    ░░  ░░    ░░    ░░ ░░░  ░░  ░░
"

" Source additional files, I did not inlude cuz they were too verbouse
runtime! config/*


" ▄▖          ▜   ▄▖  ▗ ▗ ▘
" ▌ █▌▛▌█▌▛▘▀▌▐   ▚ █▌▜▘▜▘▌▛▌▛▌▛▘
" ▙▌▙▖▌▌▙▖▌ █▌▐▖  ▄▌▙▖▐▖▐▖▌▌▌▙▌▄▌
"                            ▄▌
"================================
" Line numbers
set number
" horizontal cursor line
set cursorline
" system clipboard as default register
set clipboard+=unnamedplus
" make search only case sensitive if a upper case character is in search
" string
set ignorecase smartcase
" Autoindenting
set smartindent smarttab

set conceallevel=2
" 2 spaces tab and tab as spaces
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4  " Let backspace delete indent
set whichwrap+=<,>,h,l  " Allow backspace and cursor keys to cross line boundaries
set number relativenumber " Hybrid Line Numbers
set list " Show trailing spaces and tabs
" relative numbers and relative numbers when Focus, otherwise absolute ones
autocmd WinEnter * set relativenumber cursorline
autocmd WinLeave * set norelativenumber nocursorline
" Ask before hiding buffers with modified change
set confirm
" Keep 1 lines after cursor on screen
set scrolloff=1
" no backups no writebackups and swapfiles, cuz CoC does not like it and neither do i
set nobackup nowritebackup noswapfile
" Display title
set title
" autoCD into file directory
set autochdir
" persistend UNDO
set undodir="~/.cache/nvim/undodir"
set undofile
set timeoutlen=300 " timeout for jk and vim-which key

" start searching before pressing enter
set incsearch

set inccommand=nosplit " preview subsitude commands

" Spellchecking
set spelllang=de_de
set spellfile="~/.config/nvim/spell/de.utf-8.add"
set spellsuggest=best,10
" set spell

" ▄▖▜     ▘
" ▙▌▐ ▌▌▛▌▌▛▌▛▘
" ▌ ▐▖▙▌▙▌▌▌▌▄▌
"       ▄▌
"===============


let g:coc_global_extensions = [
\ 'coc-highlight',
\ 'coc-pairs',
\ 'coc-pyright',
\ 'coc-json',
\ 'coc-xml',
\ ]

call plug#begin('~/.vim/plugged')


Plug 'vim-airline/vim-airline'
Plug 'junegunn/fzf'							" fzf :)
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-commentary'					" Commenter Pluging if comment not supported use autocmd FileType apache setlocal commentstring=#\ %s
Plug 'neoclide/coc.nvim', {'branch': 'release'}			" Completion engine
Plug 'ryanoasis/vim-devicons' 					" Do i really have to explain?
Plug 'habamax/vim-godot'
Plug 'junegunn/goyo.vim' 						" Center vim 
Plug 'skywind3000/asyncrun.vim' 				" Async command output
Plug 'tridactyl/vim-tridactyl'
Plug 'DumbMahreeo/vim-deep-space'
Plug 'junegunn/vim-peekaboo' 					" Register siderbar
" Git related
Plug 'tpope/vim-fugitive'
Plug 'idanarye/vim-merginal'
Plug 'junegunn/gv.vim'						"Git repo browser
Plug 'jreybert/vimagit'
Plug 'mhinz/vim-startify'
Plug 'tpope/vim-surround'
"==========================
" Markdown experiments
" Plug 'plasticboy/vim-markdown'
" Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
" =========================
" Notetaking experiments
Plug 'Yggdroot/indentLine'
Plug 'matze/vim-move'
Plug 'liuchengxu/vim-which-key'

call plug#end()

" indentLine {
let g:indentLine_char_list = ['|', '¦', '┆', '┊']
" }

" FZF {
" Customize fzf colors to match your color scheme
" - fzf#wrap translates this to a set of `--color` options
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }
" }

" Goyo {
let g:goyo_width = 130
let g:goyo_height= "100%"
" }

" asyncrun.vim {
let g:asyncrun_open = 1
" }

" airline {
" Tabline extension
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_buffers = 0  " needed for tab_min_count
let g:airline#extensions#tabline#tab_min_count = 2 " Hide tabline when only one buffer
let g:airline#extensions#tabline#formatter = "unique_tail" " Only show filename not path


let g:airline#extensions#whitespace#enabled = 0
let g:airline#extensions#wordcount#enabled = 1
" }



" vim-startify {
let g:startify_bookmarks = [ {'c': '~/.config/nvim/init.vim'}, {"z":'~/.zshrc'}]
" }

" ▄▖▌
" ▐ ▛▌█▌▛▛▌█▌
" ▐ ▌▌▙▖▌▌▌▙▖
"============
"
" Use all the colors
set termguicolors
colorscheme deep-space

" Dont show -- Insert -- at bottom
set noshowmode

let g:airline_theme='deep_space'

" ▌     ▌ ▘   ▌▘
" ▙▘█▌▌▌▛▌▌▛▌▛▌▌▛▌▛▌▛▘
" ▛▖▙▖▙▌▙▌▌▌▌▙▌▌▌▌▙▌▄▌
"     ▄▌          ▄▌
"=====================
" Unmap Space and set it to leader
nnoremap <Space> <NOP>
let g:mapleader = "\<Space>"
let g:maplocalleader = ','


" Which Key {
" Define prefix dictionary

call which_key#register('<Space>', "g:leader_key_map")
call which_key#register(',', "g:localleader_key_map")
nnoremap <silent> <leader>      :<c-u>WhichKey '<Space>'<CR>
nnoremap <silent> <localleader> :<c-u>WhichKey  ','<CR>
nnoremap <silent> z      :<c-u>WhichKey 'z'<CR>

let g:leader_key_map =  {}
let g:localleader_key_map =  {}
" }


" Normal Mode
" =============
" Window Movement
nnoremap <Leader>w <C-W>

" File Control {
let g:leader_key_map.f = { 'name': '+file' }

let g:leader_key_map.f.s = 'save'
nnoremap <Leader>fs :w<CR>

let g:leader_key_map.f.S = 'save with root permissions'
nnoremap <silent> <Leader>fS :w !doas tee %<CR>

let g:leader_key_map.f.r = 'recent'
nnoremap <Leader>fr :History<CR>

let g:leader_key_map.f.p = { 'name': '+private/config' } " {
let g:leader_key_map.f.p.s = 'source'
nnoremap <Leader>fps :w<CR>:so $MYVIMRC<CR>

let g:leader_key_map.f.p.e = 'edit'
nnoremap <Leader>fpe :e $MYVIMRC<CR>
let g:leader_key_map.f.p.f = 'find'
nnoremap <Leader>fpf :cd ~/.config/nvim/<CR>:Files<CR>
"       }
"}


" Splits
nnoremap <Leader>wv :vsplit<CR>
nnoremap <Leader>ws :split<CR>
nnoremap <Leader>wd :close<CR>

" Buffer Control {
nnoremap <Leader>bn :bn<CR>
nnoremap <Leader>bp :b#<CR>
nnoremap <Leader>bb :Buffers<CR>
nnoremap <Leader>bl :Lines<CR>
nnoremap <Leader>bd :bd<CR>
" }

" Plug control
nnoremap <Leader>pi :PlugInstall<CR>
nnoremap <Leader>pu :PlugUpdate<CR>

" vim control
nnoremap <Leader>qq :qa<CR>

" PWD control
nnoremap <Leader>cd :cd %:h<CR>
nnoremap <Leader>ch :cd ~<CR>

" CHADTREE
nnoremap <Leader>of <CMD>CHADopen<CR>

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" Toggle Goyo
nnoremap <Leader>tg :Goyo<CR>

" Quickfix window, resize cuz Goyo
nnoremap <Leader>qc :cclose<CR>:resize<CR>
nnoremap <Leader>qo :copen<CR>

" git bindings
nnoremap <Leader>gc :GV<CR>
nnoremap <Leader>gb :Merginal<CR>
nnoremap <Leader>gg :MagitOnly<CR>

" fzf files
nnoremap <Leader><Leader> :Files<CR>

" Emacs Alt + x
nnoremap <A-x> :Commands<CR>

"Help bindings
nnoremap <Leader>hm :Maps<CR>
nnoremap <Leader>hh :Helptags<CR>

" vimwiki shortcuts
nnoremap <localleader>e :w<CR>:Vimwiki2HTML<CR>

" Spell correction with fzf
function! FzfSpellSink(word)
  exe 'normal! "_ciw'.a:word
endfunction
function! FzfSpell()
  let suggestions = spellsuggest(expand("<cword>"))
  return fzf#run({'source': suggestions, 'sink': function("FzfSpellSink"), 'down': 10 })
endfunction
nnoremap z= :call FzfSpell()<CR>

" =============
" Insert Mode
" =============
" Dont hit escape! hit jk
imap jk <ESC>


" =============
" Visual Mode
" =============
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
xmap ga <Plug>(EasyAlign)

vnoremap <Leader>gv :GV?<CR>

" ==============
" Terminal Mode
" ==============
tnoremap jk <C-\><C-n>
tnoremap <Esc> <C-\><C-n>


" ▄▖▘▜   ▗         ▄▖  ▗ ▗ ▘      
" ▙▖▌▐ █▌▜▘▌▌▛▌█▌  ▚ █▌▜▘▜▘▌▛▌▛▌▛▘
" ▌ ▌▐▖▙▖▐▖▙▌▙▌▙▖  ▄▌▙▖▐▖▐▖▌▌▌▙▌▄▌
"          ▄▌▌                ▄▌  
"=================================
" Godot
func! GodotSettings() abort
    setlocal foldmethod=expr
    setlocal tabstop=4
    nnoremap <buffer> <F4> :GodotRunLast<CR>
    nnoremap <buffer> <F5> :GodotRun<CR>
    nnoremap <buffer> <F6> :GodotRunCurrent<CR>
    nnoremap <buffer> <F7> :GodotRunFZF<CR>
endfunc
augroup godot | au!
    au FileType gdscript call GodotSettings()
augroup end
