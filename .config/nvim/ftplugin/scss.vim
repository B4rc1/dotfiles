" For scss files, you may need use:
" `autocmd FileType scss setl iskeyword+=@-@`
" in your vimrc for add @ to iskeyword option.
" -- https://github.com/neoclide/coc-css
setl iskeyword+=@-@
