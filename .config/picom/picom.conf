#################################
#          Animations           #
#################################
# requires https://github.com/jonaburg/picom
# (These are also the default values)
transition-length = 300
transition-pow-x = 0.07
transition-pow-y = 0.07
transition-pow-w = 0.1
transition-pow-h = 0.1
size-transition = true

#################################
#             Shadows           #
#################################

shadow = false;
shadow-radius = 5;
shadow-opacity = 1;
shadow-offset-x = 0;
shadow-offset-y = 0;

# shadow-red = 10
# shadow-green = 2
# shadow-blue = 10
# shadow-ignore-shaped = ''
shadow-exclude = [
  "name = 'Notification'",
  "class_g ?= 'Notify-osd'",
  "class_g = 'Cairo-clock'",
  "_GTK_FRAME_EXTENTS@:c"
];

#################################
#           Fading              #
#################################

fading = true;
fade-delta = 3;
fade-in-step = 0.05;
fade-out-step = 0.01;
fade-exclude = [
    "!class_g = 'eww-vol'",
];

#################################
#   Transparency / Opacity      #
#################################

inactive-opacity = 1;
active-opacity = 1;
frame-opacity = 1;
inactive-opacity-override = false;
#inactive-dim = 0.2;
# menu-opacity = 0.1; # does not work in chromium based applications

# opacity-rule = [ "80:_IS_FLOATING@:32c = 1" ];


inactive-dim-fixed = true;

#################################
#     Background-Blurring       #
#################################
# blur-background = true;
# blur-method = "kawase";
# blur-strength = 5;
# #important
# blur-deviation = 70;
# blur-background-frame = true;
# blur-background-fixed = true;
# 
# blur-background-exclude = [
#   "window_type = 'dock'",
#   "window_type = 'desktop'"
# ];

#################################
#       General Settings        #
#################################

experimental-backends = true;
backend = "glx";
vsync = true;
dbe = false;

mark-wmwin-focused = true;
mark-overdir-focused = true;

# detect-rounded-corners = true;
corner-radius = 15.0;
round-borders = 1;

rounded-corners-exclude = [
  "(!_IS_FLOATING@:32c = 1 || class_g = 'eww-vol')",
];


detect-client-opacity = true;
detect-transient = true;
detect-client-leader = true;
# enabling causes blur / opacity artifacts
use-damage = false;
refresh-rate = 0;

glx-no-stencil = false;
glx-copy-from-front = false;
glx-no-rebind-pixmap = false;
xrender-sync-fence = true;

wintypes:
{
# 	tooltip = {
#  		fade = true; 
# 		shadow = false; 
# 		focus = true; 
# 		full-shadow = false;
#  	};
   	 dock = {
 		fade = true;
 	}
#   	dnd = {
# 		shadow = false;
# 	 }
};
