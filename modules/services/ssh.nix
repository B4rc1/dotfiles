{ options, config, lib, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.ssh;
in {
  options.modules.services.ssh = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.openssh = {
      enable = true;
      settings = {
        KbdInteractiveAuthentication = false;
        PasswordAuthentication = false;
      };
    };

    user.openssh.authorizedKeys.keys =
      if config.user.name == "jonas"
      then [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDONQ7uffjNgMUDJu6f3DtwC7omALPch5XnpZotQ0xFfaxzwpx0dViIv1jb+LV3s0d1PpRqpFo5nq+nHJozJS9p+BvlGAOLkvjQ0xZ9G7SMS1fvNRQROrhr3z4gwJgqgJKeyKYv6z1f+tzW6FiQZHlfoi3ox3sPIPjMj1efCm7d0HHz6ZOD+rsDOQCCD/bxvgIJ4q82xc58+sgzyqrpQqadwft9GXoOLaubWV/wTK4kE6wCxwS9vcgZIGTXiBQpLr48LimN0gS1l0xPTH2omYsTeGdo5XPG5wksSDMUk9qpzDTf7tuw4sGYw6wPudsP1bS8NUUGi7oDYNnTUYxgsCluwEfhCLqcHlL0DDZ1Lf1ucABiWUkPHyJHk6tli+b5m0iQq4OFxGYPv/5UTYZH3T0hmY7yRyxtBmqepYKJmx2TAQ+awblyzg1DmE8ifdMQ7qW+zuUCn/snbPpPROn/raxmS+kJjeuPfB/C6D8Zr2OiaZZ1jGhHT2JCt9dqWM2LOSs= jonas@jonas-pc"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDaeX0RXBzPxjBabfd+o8w/2J9MTL6kE7ks/X/sLMOMAC7iY7nigxf2/MIzLp0TczG6hx6G7/xcyvH7neWtxSLtEt7AG2wygFdy87MJRl+2xMvIZ/qScMfbz064LOeTcTKUhEbvaSt29o/MBo+5PlatkVHYmb1fxht7F6lrWt+18x0THwa6Vk1rALDix52kFVa6gagHpuJHEMxz526MnJFeJokTIoXqHf1xTpbDPigBzrpNs1PDtnDWHZmDLdwCEzv945qa71/C6moaMtHPsF3rbfV7ECtWwx2GkahAX1RR45SF/75FSnEZp4bGn/THTqGpMLk54QyMGLw7cb8QIEY+bNGW1V4KdOSoDyt5wv9MsgvI5k3wdlT9NZJzu/MhWUgI0qxicG959TLgkqTP1V9gnn5Mof6hWuX6dNWwZXixqhdEjiFXw+EaGKT5u05zmLW8JPu5PRWd9b5conV00IUG6d5jKPO3LerfiC62p/8xYPmP4xPB0XSVTOeOKsKCJGk= jonas@thonkpad"
      ]

      else [];
  };
}
