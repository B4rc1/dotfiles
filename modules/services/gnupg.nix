{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.services.gnupg;
    configDir = config.dotfiles.configDir;
in {
  options.modules.services.gnupg = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      pinentry
    ];

    services.pcscd.enable = true;
    programs.gnupg.agent = {
      enable = true;
    };
  };
}
