{ pkgs, ... }:
let
  nixos-diff = pkgs.writeShellScriptBin "nixos-diff" ''
    nixos-rebuild build "$@" && nvd diff /run/current-system result && while true; do
      read -p "Is Ok? (Y/N)" yn
      case $yn in
          [Yy]* ) break ;;
          [Nn]* ) exit ;;
          * ) echo "Please answer yes or no.";;
      esac
  done && sudo nixos-rebuild switch
  '';
in
{
  environment.systemPackages = [
    nixos-diff
  ];
  environment.shellAliases = {
    # little wrapper around nixos-rebuild switch that warns, if there are untracked files,
    # because I keep forgetting to add files to my flake git repo.
    # rebuild = ''
    #   ufiles=$(git status --porcelain 2>/dev/null| grep "^??"); [[ -z $ufiles ]] && nixos-rebuild switch || echo -e "\e[1m\e[31mUNTRACKED FILES:\e[39m\e[0m\n $ufiles"
    # '';
    econf = "nvim /etc/nixos/hosts/$(cat /etc/hostname)/default.nix";
  };
}
