{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.editors.neovim;
    configDir = config.dotfiles.configDir;

    nvimWrapperScript = pkgs.writeShellScriptBin "nvim-term" ''
        "${config.modules.desktop.term.default}" -e ${pkgs.unstable.neovim}/bin/nvim "$@"
    '';

    neovideWrapperScript = pkgs.writeShellScriptBin "nvim-nvr" ''
      export NVR_CMD=neovide
      ${pkgs.unstable.neovim-remote}/bin/nvr --remote-tab "$@"
    '';

    mkLspconfigSetup = { name, extraSetup ? "", preSetup ? ""}: ''
      ${preSetup}
      lspconfig["${name}"].setup({
        on_attach = on_attach,
        capabilities = capabilities,
        ${extraSetup}
      })
    '';

in {
  options.modules.editors.neovim = with types; {
    enable = mkBoolOpt true;

    lspconfig.servers = mkOpt' (listOf (attrsOf str)) [] ''
      Array of Servers to configure from https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md.

      Each Configuration in the List has to have 1 attribute: 'name'. This is the Name in server_configurations.md.
      Optionally each Server may have an attribute 'extraSetup' which contains lines to be appendend to the setup
      table.
      Additionally the Attribute 'preSetup' may be used to execute arbitrary lua before evaluating the table. e.g. to precompute
      values.
    '';
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      unstable.neovim
      unstable.neovide
      unstable.tree-sitter
      nodejs

      # neovim remote
      unstable.neovim-remote
      (pkgs.makeDesktopItem {
        name = "nvim-term";
        desktopName = "Neovim";
        exec = "${nvimWrapperScript}/bin/nvim-term";
        genericName = "Text Editor";
        icon = "nvim";
      })

      (pkgs.makeDesktopItem {
        name = "nvim-nvr";
        desktopName = "Neovim NVR";
        exec = "${neovideWrapperScript}/bin/nvim-nvr";
        genericName = "Text Editor";
        icon = "nvim";
      })

      # for treesitter:
      gcc libgccjit
      # for lsp:
      # unstable.rnix-lsp
      # unstable.nil
      unstable.nixd
      unstable.lua-language-server
      nodePackages.bash-language-server
      shellcheck

      # telescop-frecency
      sqlite

      # formattin
      nixpkgs-fmt

      # my buildsystem scripting
      unstable.nushell

      luarocks
    ];

    environment.variables = {
      NEOVIDE_MULTIGRID = "1";
      MANPAGER = lib.mkForce "nvim +Man!";
    };

    environment.shellAliases = {
      edit    = "nvr";
      split   = "nvr -o";
      vsplit  = "nvr -O";
      tabedit = "nvr --remote-tab";
    };


    modules.editors.neovim.lspconfig.servers = [
      { name = "nixd"; }
      { name = "bashls"; }
      { name = "nushell"; }
      {
        name = "lua_ls";
        preSetup = ''
          local runtime_path = {};
          table.insert(runtime_path, "lua/?.lua")
          table.insert(runtime_path, "lua/?/init.lua")

          local library_path = {}
          ${if config.modules.desktop.awesome.enable then
            "table.insert(library_path, \"${pkgs.awesome}/share/awesome/lib\")" else ""}
        '';
        extraSetup = ''
          settings = {
            Lua = {
              runtime = {
                version = 'LuaJIT',
                -- path = runtime_path,
              },
              IntelliSense = {
                traceBeSetted = true,
                traceFieldInject = true,
                traceLocalSet = true,
                traceReturn = true,
              },
              workspace = {
                preloadFileSize = 500,
                library = library_path,
              },
              diagnostics = {
                workspaceDelay = 0,
                globals = {
                  -- vim
                  "vim",
                  "use",
                  -- AwesomeWM
                  "awesome",
                  "screen",
                  "client",
                  "root"
                },
              },
              telemetry = {
                enable = false,
              },
            }
          }
        '';
      }
    ];

    home.configFile = {
      "nvim" = {
        source = "${configDir}/nvim";
        recursive = true;
      };
      "nvim/lua/nixpaths.lua".text = ''
        -- This file was auto generated by modules/editors/neovim.nix.
        -- DO NOT EDIT!

        return {
          sqlite = '${pkgs.sqlite.out}',
        }
      '';

      "nvim/lua/generated_serverconfigs.lua".text = ''
        -- all lsp servers are defined here so they can be toggled from nix.

        -- This file was auto generated by modules/editors/neovim.nix.
        -- DO NOT EDIT!

        ${concatStringsSep "\n"
          (map mkLspconfigSetup cfg.lspconfig.servers)
        }
      '';
    };
    system.userActivationScripts.cleanupNvim = "rm -fv $XDG_DATA_HOME/nvim/site/plugin/packer_compiled.lua";
  };
}
