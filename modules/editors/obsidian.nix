{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.editors.obsidian;
    configDir = config.dotfiles.configDir;
    obsidian_pkg = pkgs.unstable.obsidian;
in {
  options.modules.editors.obsidian = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      librsvg # for obsidian pdf export
      plantuml # for diagrams
      obsidian_pkg
    ];

    environment.systemPackages = [
      (pkgs.makeDesktopItem {
        name = "obsidian-personal-vault";
        desktopName = "Personal Vault";
        exec = "${obsidian_pkg}/bin/obsidian  \"obsidian://open?vault=personal\"";
        genericName = "Obsidan";
        icon = "obsidian";
      })
      (pkgs.makeDesktopItem {
        name = "obsidian-informatik-vault";
        desktopName = "Informatik Vault";
        exec = "${obsidian_pkg}/bin/obsidian \"obsidian://open?vault=informatik-obsidian\"";
        genericName = "Obsidan";
        icon = "obsidian";
      })
    ];
  };
}
