{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.rofi;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.apps.rofi = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    home.configFile."rofi" = {
      source = "${configDir}/rofi";
      recursive = true;
    };
  };
}
