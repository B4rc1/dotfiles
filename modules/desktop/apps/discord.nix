{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.discord;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.apps.discord = {
    enable = mkBoolOpt false;
    autostart = mkBoolOpt true;
  };

  config = mkIf cfg.enable (mkMerge [
  {
    user.packages = with pkgs; [
      discord
    ];
  }
  (mkIf cfg.autostart {
    modules.desktop.autostart.postNetwork = 
    ''
      if pgrep -x ".Discord-wrappe" > /dev/null
      then
        echo "Discord running"
      else
        echo "discord not running, starting"
        ${pkgs.discord}/bin/discord >/dev/null &
      fi
    '';
  })
  ]);
}
