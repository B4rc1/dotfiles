{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.virt-manager;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.apps.virt-manager = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      virt-manager
    ];

    virtualisation.libvirtd.enable = true;
    programs.dconf.enable = true;
  };
}
