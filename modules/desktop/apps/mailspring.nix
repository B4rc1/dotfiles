{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.apps.mailspring;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.apps.mailspring = {
    enable = mkBoolOpt false;
    autostart = mkBoolOpt true;
  };

  config = mkIf cfg.enable (mkMerge [
  {
    user.packages = with pkgs; [
      unstable.mailspring
    ];

    programs.seahorse.enable = true;
    services.gnome.gnome-keyring.enable = true;
    programs.ssh.startAgent = true;

    services.xserver.displayManager.sessionCommands = ''
      systemctl --user import-environment DISPLAY XAUTHORITY
      eval $(${pkgs.gnome-keyring}/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)
      export SSH_AUTH_SOCK=
    '';
  }

  (mkIf cfg.autostart {
    modules.desktop.autostart.postNetwork = 
    ''
      if pgrep -x "mailspring" > /dev/null
      then
        echo "mailspring running"
      else
        echo "mailspring not running, starting"
        # FIXME: find a better way to fix gnome-libsecret
        ${pkgs.unstable.mailspring}/bin/mailspring --password-store="gnome-libsecret" > /dev/null 2>&1 &
      fi
    '';
  })
  ]);
}
