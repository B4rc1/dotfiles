{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.awesome;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.awesome = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {

    nixpkgs.overlays = [
      (self: super: {
        awesome = super.awesome.override { lua = super.luajit; };
      })
    ];

    user.packages = with pkgs; [
      psmisc # pstree, required by window swalloing

      volctl
    ];

    services.displayManager = {
      defaultSession = "none+awesome";
      autoLogin = {
        enable = true;
        user = config.user.name;
      };
    };
    services.xserver = {
      windowManager.awesome = {
        enable = true;
        # luaModules = with pkgs.luaPackages; [
        #   luarocks # is the package manager for Lua modules
        #   luadbi-mysql # Database abstraction layer
        # ];
      };
    };

    services.picom = {
      enable = true;
      settings = {
        # I'm using a picom fork, so this is needed.
        experimental-backends = true;

        # --- Transitions ---
        transition-length = 300;
        transition-pow-x = 0.07;
        transition-pow-y = 0.07;
        transition-pow-w = 0.1;
        transition-pow-h = 0.1;
        size-transition = true;


        # --- Fade ---
        fading = true;
        fade-delta = 3;
        fade-in-step = 0.05;
        fade-out-step = 0.01;


        # --- misc ---
        vsync = true;
        backend = "glx";
        glx-no-stencil = false;
        glx-copy-from-front = false;
        glx-no-rebind-pixmap = false;
        xrender-sync-fence = true;

      };
    };

    home.configFile = {
      "awesome" = {
        source = "${configDir}/awesome";
        recursive = true;
      };
    };
  };
}
