{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.browsers.brave;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.browsers.brave = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      brave
    ];

    # home.configFile = {
    #   "example/example".source = "${configDir}/example/example";
    # };
  };
}
