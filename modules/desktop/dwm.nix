{ options, config, lib, pkgs, inputs, ... }:


with lib;
with lib.my;
let cfg = config.modules.desktop.dwm;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.dwm = { 
    enable = mkBoolOpt false;
    autostart = {
      preNetwork = mkOpt' types.lines "" ''
        Bash lines to be written to ~/.config/dwm/autostart.sh, executed
        before network is available
      '';
      postNetwork = mkOpt' types.lines "" ''
        Bash lines to be written to ~/.config/dwm/autostart.sh, executed
        after network is available
      '';
    };
  };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      dwm

      sxhkd

      dunst

      # dwm dependencies already defined in modules/desktop/default.nix:
      # - thunar
      # - pavucontrol
      # - rofi

      # dwm dependencies already defined in toplevel default.nix:
      # - gotop
    ];

    fonts.packages = with pkgs; [
      envypn-font siji
    ];

    systemd.user.services."dunst" = {
      enable = true;
      description = "";
      wantedBy = [ "default.target" ];
      serviceConfig.Restart = "always";
      serviceConfig.RestartSec = 2;
      serviceConfig.ExecStart = "${pkgs.dunst}/bin/dunst";
    };

    systemd.user.services."sxhkd" = {
      enable = true;
      description = "";
      wantedBy = [ "default.target" ];
      serviceConfig.Restart = "always";
      serviceConfig.RestartSec = 2;
      serviceConfig.ExecStart = "${pkgs.sxhkd}/bin/sxhkd";
    };

    services = {
      displayManager = {
        defaultSession = "none+dwm";
        autoLogin = {
          enable = true;
          user = config.user.name;
        };
      };
      xserver = {
        windowManager.session = singleton
        {
          name = "dwm";
          start =
            # NOTE: this cannot use modules.desktop.autostart.preNetwork since it starts before dwm.
            ''
              dwm &
              waitPID=$!

              $DOTFILES/bin/dwm/bar.sh &
            '';

        };
      };
      picom = {
        enable = true;
        settings = {
          # I'm using a picom fork, so this is needed.
          experimental-backends = true;

          # --- Transitions ---
          transition-length = 300;
          transition-pow-x = 0.07;
          transition-pow-y = 0.07;
          transition-pow-w = 0.1;
          transition-pow-h = 0.1;
          size-transition = true;


          # --- Fade ---
          fading = true;
          fade-delta = 3;
          fade-in-step = 0.05;
          fade-out-step = 0.01;


          # --- misc ---
          vsync = true;
          backend = "glx";
          glx-no-stencil = false;
          glx-copy-from-front = false;
          glx-no-rebind-pixmap = false;
          xrender-sync-fence = true;

        };
      };
    };

    home.configFile = {
      "sxhkd/sxhkdrc" = {
        source ="${configDir}/sxhkd/sxhkdrc";
        onChange = "pkill -SIGUSR1 sxhkd";
      };
    };
  };
}
