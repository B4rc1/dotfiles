{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.media.audiobooks;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.media.audiobooks = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      cozy
    ];

    # home.configFile = {
    #   "example/example".source = "${configDir}/example/example";
    # };
  };
}
