{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.media.spotify;
in {
  options.modules.desktop.media.spotify = {
    enable = mkBoolOpt false;
    autostart = mkBoolOpt true;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      user.packages = with pkgs; [
        spotify
      ];
    }

    (mkIf cfg.autostart {
      modules.desktop.autostart.postNetwork = 
      ''
        if pgrep -x ".spotify-wrappe" > /dev/null
        then
          echo "Spotify running"
        else
          echo "spotify not running, starting"
          ${pkgs.spotify}/bin/spotify > /dev/null 2>&1 &
        fi
      '';
    })
  ]);
}
