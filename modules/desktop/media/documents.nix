{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.media.documents;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.media.documents = {
    enable = mkBoolOpt false;
    graphicsTablet.enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      user.packages = with pkgs; [
        zathura
        pandoc
        pdftk
      ];

      home.configFile = {
        "zathura/zathurarc".source = "${configDir}/zathura/zathurarc";
      };
    }

    (mkIf cfg.graphicsTablet.enable {
      hardware.opentabletdriver = {
        enable = true;
        package = pkgs.unstable.opentabletdriver;
      };
      environment.systemPackages = with pkgs; [
        xournalpp
      ];
    })
  ]);
}
