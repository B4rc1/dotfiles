{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.media.mpv;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.media.mpv = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      mpv
      yt-dlp

      # Since a lot of mpv scripts/plugins depend on it.
      # My usecase right now is 'mpv-cut' [1]
      #
      # [1]: https://github.com/familyfriendlymikey/mpv-cut
      ffmpeg
    ];

    nixpkgs.overlays = [
      (self: super: {
        mpv = super.mpv.override {
          scripts = with pkgs.mpvScripts; [
            thumbfast
            uosc
            mpv-cheatsheet
            videoclip
          ];
        };
      })
    ];

    home.configFile = {
      "mpv/mpv.conf".source = "${configDir}/mpv/mpv.conf";
    };
  };
}
