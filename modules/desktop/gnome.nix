{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.gnome;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.gnome = {
    enable = mkBoolOpt false;
  };
  config = mkIf cfg.enable {

    services.xserver.enable = true;
    services.xserver.displayManager.gdm.enable = true;
    services.xserver.desktopManager.gnome.enable = true;

    programs.dconf.enable = true;
    hardware.pulseaudio.enable = false;

    environment.systemPackages = with pkgs; [ 
      dracula-theme
      chrome-gnome-shell
      gnome3.gnome-tweaks
      gnome-extension-manager
      gnome3.adwaita-icon-theme
      gnome3.gnome-settings-daemon

      gnomeExtensions.appindicator

      gnomeExtensions.vitals
      gnomeExtensions.tiling-assistant
    ];

    services.dbus.packages = with pkgs; [ gnome2.GConf ];

    # systray
    services.udev.packages = with pkgs; [ gnome.gnome-settings-daemon ];
  };
}
