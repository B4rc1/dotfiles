{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.gaming.steam;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.gaming.steam = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    programs.steam.enable = true;

    user.packages = with pkgs; [
      steamcmd
    ];

   # better for steam proton games
    systemd.extraConfig = "DefaultLimitNOFILE=1048576";
  };
}
