{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop = {
    autostart = {
      preNetwork = mkOpt' types.lines "" ''
        commands to autostart as soon as the user logs in
      '';
      postNetwork = mkOpt' types.lines "" ''
        commands to autostart as soon as network is available
      '';
    };
  };

  config = mkIf config.services.xserver.enable {
    assertions = [
      {
        assertion = (countAttrs (n: v: n == "enable" && value) cfg) < 2;
        message = "Can't have more than one desktop environment enabled at a time";
      }
      {
        assertion =
          let srv = config.services;
          in srv.xserver.enable ||
             !(anyAttrs
               (n: v: isAttrs v &&
                      anyAttrs (n: v: isAttrs v && v.enable))
               cfg);
        message = "Can't enable a desktop app without a desktop environment";
      }
    ];

    environment.systemPackages = with pkgs; [
      handlr
      (pkgs.writeShellScriptBin "xdg-open" "handlr open \"$@\"")

      xclip
      xdotool

      xmousepasteblock
      pavucontrol

      (rofi.override { plugins = [ rofi-emoji ]; })

      arandr
      sxiv
      imagemagick

      dmenu

      maim
      pamixer
      playerctl

      (xfce.thunar.override {
        thunarPlugins = with pkgs; [
          # mount and unmount drives and media
          xfce.thunar-volman
          # archive managment
          xfce.thunar-archive-plugin
          ];
      })
      # thunar dependencies {{
        # xfconf is needed to save settings
        xfce.xfconf
        # tumbler is needed for thumbnails
        xfce.tumbler
        # archive manager
        file-roller
      # }}

      graphviz

      libnotify # notify-send

      zenity # dialog

      speedcrunch # my calculator

      libsForQt5.qtstyleplugins
    ];

    # Network file and Trash abstraction. Needed
	  services.gvfs.enable = true;

    security.polkit.extraConfig = ''
      /* Allow any local user to do anything (dangerous!). */
      polkit.addRule(function(action, subject) {
        if (subject.local) return "yes";
      });
    '';

    # since this needs to be mutable, we just copy it if it does not exist.
    system.userActivationScripts.copyThunarDefaults = ''
      if [[ ! -d $XDG_CONFIG_HOME/Thunar/ ]]; then
        mkdir $XDG_CONFIG_HOME/Thunar
        cp ${configDir}/thunar/* $XDG_CONFIG_HOME/Thunar/
        chmod +w $XDG_CONFIG_HOME/Thunar/uca.xml
      fi
    '';

    fonts = {
      fontDir.enable = true;
      enableGhostscriptFonts = true;
      packages = with pkgs; [
        (nerdfonts.override { fonts = [ "Iosevka" "Hack" ]; })
      ];
    };

    ## Apps/Services
    # Configure keymap in X11
    services.xserver.xkb = {
      layout = "de";
      variant = "nodeadkeys";
    };

    services.redshift = {
      enable = true;
      temperature = {
        day = 6500;
        night = 3200;
      };
      brightness = {
        day = "1.0";
        night = "0.6";
      };
    };
    location = {
      latitude = 50.775345;
      longitude  = 6.083887;
    };


    # Select internationalisation properties.
    i18n.defaultLocale = "en_US.utf8";

    i18n.extraLocaleSettings = {
      LC_ADDRESS = "de_DE.utf8";
      LC_IDENTIFICATION = "de_DE.utf8";
      LC_MEASUREMENT = "de_DE.utf8";
      LC_MONETARY = "de_DE.utf8";
      LC_NAME = "de_DE.utf8";
      LC_NUMERIC = "de_DE.utf8";
      LC_PAPER = "de_DE.utf8";
      LC_TELEPHONE = "de_DE.utf8";
      LC_TIME = "de_DE.utf8";
    };


    # Try really hard to get QT to respect my GTK theme.
    env.GTK_DATA_PREFIX = [ "${config.system.path}" ];

    qt = {
      enable = true;
      platformTheme = "qt5ct";
    };


    # FIXME: Use upstream ulauncher as soon as https://github.com/NixOS/nixpkgs/pull/196410 gets merged
    modules.desktop.autostart.preNetwork = ''
      dbus-update-activation-environment --systemd DBUS_SESSION_BUS_ADDRESS DISPLAY XAUTHORITY
      ${pkgs.xmousepasteblock}/bin/xmousepasteblock &
    '';

    services.xserver.displayManager.sessionCommands = ''
      # GTK2_RC_FILES must be available to the display manager.
      export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"

      (${cfg.autostart.preNetwork}) &
      (while ! ping -c1 google.com; do sleep 0.1; echo " [autostart]: network not available... retrying..."; done
      ${cfg.autostart.postNetwork}) &

      thunar --daemon &
    '';
  };
}
