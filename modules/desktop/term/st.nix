# modules/desktop/term/st.nix

{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.term.st;
in {
  options.modules.desktop.term.st = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    # TODO: Investigate what this is about
    # xst-256color isn't supported over ssh, so revert to a known one
    # modules.shell.zsh.rcInit = ''
    #   [ "$TERM" = xst-256color ] && export TERM=xterm-256color
    # '';

    fonts.packages = with pkgs; [
      envypn-font
      unifont unifont_upper
      cozette
    ];

    user.packages = with pkgs; [
      xst  # st + nice-to-have extensions
      (pkgs.writeShellScriptBin "xterm" "xst")

      (makeDesktopItem {
        name = "xst";
        desktopName = "Suckless Terminal";
        genericName = "Default terminal";
        icon = "utilities-terminal";
        exec = "${xst}/bin/xst";
        categories = [ "Development" "System" "Utility"];
      })
    ];
  };
}
