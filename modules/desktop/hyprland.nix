{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.hyprland;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.hyprland = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      # dunst
    ];

    programs.hyprland.enable = true;

    # services.xserver = {
    #   enable = true;
    #
    #   desktopManager = {
    #     xterm.enable = false;
    #   };
    #
    #   displayManager = {
    #     defaultSession = "none+hyprland";
    #   };
    # };


    # home.configFile = {
    #   "awesome" = {
    #     source = "${configDir}/awesome";
    #     recursive = true;
    #   };
    # };

  };
}
