{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.xmonad;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.xmonad = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      haskellPackages.xmonad
      haskellPackages.xmobar
      dunst

      scrot

      unstable.eww
      wmctrl
      alsaUtils
    ];

    environment.variables = {
      "_JAVA_AWT_WM_NONREPARENTING" = "1";
    };

    services.xserver.windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      extraPackages = haskellPackages: [
      ];
    };

    services.picom = {
      enable = true;
      settings = {
          # I'm using a picom fork, so this is needed.
          experimental-backends = true;

# --- Transitions ---
        transition-length = 300;
        transition-pow-x = 0.07;
        transition-pow-y = 0.07;
        transition-pow-w = 0.1;
        transition-pow-h = 0.1;
        size-transition = true;


# --- Fade ---
        fading = true;
        fade-delta = 3;
        fade-in-step = 0.05;
        fade-out-step = 0.01;


# --- misc ---
        vsync = true;
        backend = "glx";
        glx-no-stencil = false;
        glx-copy-from-front = false;
        glx-no-rebind-pixmap = false;
        xrender-sync-fence = true;

      };
    };

    home.configFile = {
      "xmonad" = {
        source = "${configDir}/xmonad";
        recursive = true;
      };
    };

  };
}
