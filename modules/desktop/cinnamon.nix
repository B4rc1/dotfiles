{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.cinnamon;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.cinnamon = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {

    # Enable the Cinnamon Desktop Environment.
    services.xserver.displayManager.lightdm.enable = true;
    services.xserver.desktopManager.cinnamon.enable = true;

    services.xserver.displayManager.autoLogin.enable = true;
    services.xserver.displayManager.autoLogin.user = "jonas";

    environment.systemPackages = with pkgs; [
      cinnamon.cinnamon-screensaver
      gnome.gnome-system-monitor
      gnome.gnome-screenshot
    ];

    # home.configFile = {
    #   "awesome" = {
    #     source = "${configDir}/awesome";
    #     recursive = true;
    #   };
    # };

  };
}
