{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.i3;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.i3 = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      unstable.i3-swallow
      python3Packages.i3ipc
      dunst
    ];
    environment.pathsToLink = [ "/libexec" ]; # links /libexec from derivations to /run/current-system/sw 
      services.displayManager = {
        defaultSession = "none+i3";
      };

      services.xserver = {
        enable = true;

        desktopManager = {
          xterm.enable = false;
        };


        windowManager.i3 = {
          enable = true;
          extraPackages = with pkgs; [
            dmenu #application launcher most people use
              i3status # gives you the default i3 status bar
              i3lock #default i3 screen locker
              # i3blocks #if you are planning on using i3blocks over i3status
          ];
        };
      };


  services.picom = {
    enable = true;
    settings = {
      # I'm using a picom fork, so this is needed.
      experimental-backends = true;

      # --- Transitions ---
      transition-length = 300;
      transition-pow-x = 0.07;
      transition-pow-y = 0.07;
      transition-pow-w = 0.1;
      transition-pow-h = 0.1;
      size-transition = true;


      # --- Fade ---
      fading = true;
      fade-delta = 3;
      fade-in-step = 0.05;
      fade-out-step = 0.01;


      # --- misc ---
      vsync = true;
      backend = "glx";
      glx-no-stencil = false;
      glx-copy-from-front = false;
      glx-no-rebind-pixmap = false;
      xrender-sync-fence = true;

    };
  };

    # home.configFile = {
    #   "awesome" = {
    #     source = "${configDir}/awesome";
    #     recursive = true;
    #   };
    # };

  };
}
