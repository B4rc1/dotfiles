{ config, options, lib, pkgs, ... }:


with lib;
with lib.my;
let cfg = config.modules.desktop.herbstluftwm;
    configDir = config.dotfiles.configDir;
    hlwm_pkg = pkgs.herbstluftwm;
in {
  options.modules.desktop.herbstluftwm = { enable = mkBoolOpt false; };

  config = mkIf cfg.enable {

    environment.systemPackages = with pkgs; [
      hlwm_pkg

      xorg.xkill

      # volume + media control
      dunst

      polybar
    ];

    environment.shellAliases = {
      hc = "herbstclient";
    };

    fonts.packages = with pkgs; [
      siji
      cozette
    ];

    systemd.user.services."dunst" = {
      enable = true;
      description = "";
      wantedBy = [ "default.target" ];
      serviceConfig.Restart = "always";
      serviceConfig.RestartSec = 2;
      serviceConfig.ExecStart = "${pkgs.dunst}/bin/dunst";
    };

    services.autorandr.hooks.postswitch = {
      "restart-hlwm" = ''
          ${hlwm_pkg}/bin/herbstclient reload
      '';
    };

    services = {
      # FIXME: Dont configure this here but in themes
      picom = {
        enable = true;
        settings = {
          # I'm using a picom fork, so this is needed.
          experimental-backends = true;

          # --- Transitions ---
          transition-length = 300;
          transition-pow-x = 0.07;
          transition-pow-y = 0.07;
          transition-pow-w = 0.1;
          transition-pow-h = 0.1;
          size-transition = true;


          # --- Fade ---
          fading = true;
          fade-delta = 3;
          fade-in-step = 0.05;
          fade-out-step = 0.01;


          # --- misc ---
          vsync = true;
          backend = "glx";
          glx-no-stencil = false;
          glx-copy-from-front = false;
          glx-no-rebind-pixmap = false;
          xrender-sync-fence = true;

          # --- Shadow ---
          shadow = true;
          shadow-opacity = .75;
          shadow-radius = 9;
        };
        shadowExclude = [ "! HLWM_FLOATING_WINDOW@:32c = 1" ];
        shadowOffsets = [ 5 5 ];
      };
      displayManager = {
        defaultSession = "none+herbstluftwm";
        autoLogin = {
          enable = true;
          user = config.user.name;
        };
      };
      xserver = {
        windowManager.session = singleton {
          name = "herbstluftwm";
          start = ''
            ${hlwm_pkg}/bin/herbstluftwm &
            waitPID=$!
          '';
        };

      };
    };

    # programs.seahorse.enable = lib.mkForce false;
    programs.seahorse.enable = true;


    home.configFile = {
      "herbstluftwm/autostart".source = "${configDir}/herbstluftwm/autostart";
    };
  };

}
