{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.desktop.kde;
    configDir = config.dotfiles.configDir;
in {
  options.modules.desktop.kde = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {

    services.xserver.displayManager.sddm = {
      enable = true;
      autoLogin.relogin = false; # dont automatically log me in when I log out
      wayland.enable = true;
    };
    services.xserver.desktopManager.plasma5 = {
      enable = true;
    };
    programs.kdeconnect.enable = true;

    services.redshift.enable = lib.mkForce false;
    programs.seahorse.enable = lib.mkForce false;

    # HACK: I dont know why, i should not have to wonder why, but for some #?=! reason firefox
    #       does not respect my gdk theme on plasma, so i just override it like this
    env.GTK_THEME = "Dracula:dark";

    environment.systemPackages = with pkgs; [
      libsForQt5.bismuth
    ];

    # home.configFile = {
    #   "example/example".source = "${configDir}/example/example";
    # };
  };
}
