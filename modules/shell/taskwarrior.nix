{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.taskwarrior;
    configDir = config.dotfiles.configDir;
in {
  options.modules.shell.taskwarrior = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      taskwarrior
      timewarrior
      taskopen
      python3
    ];

    environment.shellAliases = {
      t = "task";
    };

    # home.configFile = {
    #   "git/config".source = "${configDir}/git/config";
    # };

    # modules.shell.zsh.rcFiles = [ "${configDir}/git/aliases.zsh" ];
  };
}
