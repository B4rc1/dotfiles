{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.bookmarks;
    configDir = config.dotfiles.configDir;
in {
  options.modules.shell.bookmarks = {
    enable = mkBoolOpt true;
  };

  # dependencies for ./bin/mkbookmark and ./bin/get_bookmark.sh
  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      fzf
      fd
    ];
  };
}
