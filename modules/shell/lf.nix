{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.lf;
    configDir = config.dotfiles.configDir;
in {
  options.modules.shell.lf = {
    enable = mkBoolOpt true;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      unstable.lf
      # for preview script
      file ghostscript ffmpegthumbnailer atool
      # as pager and previewer
      bat
      # image previews:
      ueberzugpp

      # bulkrename
      vimv

      # wallpaper
      nitrogen

      # drag and drop
      xdragon

      # fzf_jump
      fzf fd

      # fasd_dir
      fasd

      # fzf_search
      ripgrep
    ];

    # Since I'm setting my keyboard layout dynamically and lf has race condition problems with scripts on startup [1]
    # just start lf as soon as possible.
    #
    # [1]: https://github.com/gokcehan/lf/issues/495
    modules.desktop.autostart.preNetwork =
    ''
      lf -server &
    '';


    environment.shellAliases = {
      # script that is needed for image previews. Look into ./bin/lf.sh.
      # cant call the script /bin/lf because in turn calls lf, leading to infinite recursion.
      lf = "lfcd";
    };


    modules.shell.zsh.rcInit = ''
      # lf.sh is a wrapper I wrote for image previews. See $DOTFILES/bin/lf.sh
      lfcd () {
        # `command` is needed in case `lfcd` is aliased to `lf`
        cd "$(lf.sh -print-last-dir "$@")"
      }
      zle -N lfcd
      bindkey '^O' lfcd
    '';

    environment.variables = {
      LF_ICONS = "tw=:st=:ow=:dt=:di=:fi=:ln=:or=⚠:ex=:*.7z=:*.a=:*.ai=:*.apk=:*.asm=:*.asp=:*.aup=:*.avi=:*.awk=:*.bash=:*.bat=:*.bmp=:*.bz2=:*.c=:*.c++=:*.cab=:*.cbr=:*.cbz=:*.cc=:*.class=:*.clj=:*.cljc=:*.cljs=:*.cmake=:*.coffee=:*.conf=:*.cp=:*.cpio=:*.cpp=:*.cs=:*.csh=:*.css=:*.cue=:*.cvs=:*.cxx=:*.d=:*.dart=:*.db=:*.deb=:*.diff=:*.dll=:*.doc=:*.docx=:*.dump=:*.edn=:*.eex=:*.efi=:*.ejs=:*.elf=:*.elm=:*.epub=:*.erl=:*.ex=:*.exe=:*.exs=:*.f#=:*.fifo=|:*.fish=:*.flac=:*.flv=:*.fs=:*.fsi=:*.fsscript=:*.fsx=:*.gem=:*.gemspec=:*.gif=:*.go=:*.gz=:*.gzip=:*.h=:*.haml=:*.hbs=:*.hh=:*.hpp=:*.hrl=:*.hs=:*.htaccess=:*.htm=:*.html=:*.htpasswd=:*.hxx=:*.ico=:*.img=:*.ini=:*.iso=:*.jar=:*.java=:*.jl=:*.jpeg=:*.jpg=:*.js=:*.json=:*.jsx=:*.key=:*.ksh=:*.leex=:*.less=:*.lha=:*.lhs=:*.log=:*.lua=:*.lzh=:*.lzma=:*.m4a=:*.m4v=:*.markdown=:*.md=:*.mdx=:*.mjs=:*.mkv=:*.ml=λ:*.mli=λ:*.mov=:*.mp3=:*.mp4=:*.mpeg=:*.mpg=:*.msi=:*.mustache=:*.nix=:*.o=:*.ogg=:*.pdf=:*.php=:*.pl=:*.pm=:*.png=:*.pp=:*.ppt=:*.pptx=:*.ps1=:*.psb=:*.psd=:*.pub=:*.py=:*.pyc=:*.pyd=:*.pyo=:*.r=ﳒ:*.rake=:*.rar=:*.rb=:*.rc=:*.rlib=:*.rmd=:*.rom=:*.rpm=:*.rproj=鉶:*.rs=:*.rss=:*.rtf=:*.s=:*.sass=:*.scala=:*.scss=:*.sh=:*.slim=:*.sln=:*.so=:*.sql=:*.styl=:*.suo=:*.swift=:*.t=:*.tar=:*.tex=:*.tgz=:*.toml=:*.ts=:*.tsx=:*.twig=:*.vim=:*.vimrc=:*.vue=﵂:*.wav=:*.webm=:*.webmanifest=:*.webp=:*.xbps=:*.xcplayground=:*.xhtml=:*.xls=:*.xlsx=:*.xml=:*.xul=:*.xz=:*.yaml=:*.yml=:*.zip=:*.zsh=:";
    };

    home.configFile = {
      # TODO: Move this to submodule, as soon as I know how :)
      "lf".source = "${configDir}/lf";
    };
  };
}
