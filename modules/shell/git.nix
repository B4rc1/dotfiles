{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.shell.git;
    configDir = config.dotfiles.configDir;
in {
  options.modules.shell.git = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      # gitAndTools.git-open
      delta
      difftastic
      git-lfs
      git-extras
      unstable.gitAndTools.lazygit
    ];

    environment.variables = {
      DELTA_PAGER = "bat -p";
    };

    home.configFile = {
      "git/config".source = "${configDir}/git/config";
      "lazygit/config.yml".source = "${configDir}/git/config.yml";
    };

    modules.shell.zsh.rcFiles = [ "${configDir}/git/aliases.zsh" ];
  };
}
