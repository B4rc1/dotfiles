{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.zig;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.zig = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      zig
      zls
    ];

    modules.editors.neovim.lspconfig.servers = [
      {
        name = "zls";
      }
    ];
  };
}
