{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.haskell;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.haskell = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    environment.systemPackages = with pkgs; [
      ghc
      haskell-language-server
      ormolu # formatting

      # package manager
      cabal-install
    ];

    modules.editors.neovim.lspconfig.servers = [
      {
        name = "hls";
        # this should be optional
        extraSetup = ''
          root_dir = lspconfig.util.root_pattern(".git"),
        '';
      }
    ];
  };
}
