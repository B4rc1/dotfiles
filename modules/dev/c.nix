{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.c;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.c = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      gcc
      clang
      clang-tools
    ];

    modules.editors.neovim.lspconfig.servers = [
      {
        name = "clangd";
      }
    ];
  };
}
