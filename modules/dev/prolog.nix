{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.prolog;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.prolog = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    # Note, this (as of right now) depends on "openssl-1.1.1w", which is marked EOL.
    user.packages = with pkgs; [
      swiProlog
    ];
  };
}
