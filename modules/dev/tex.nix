{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.tex;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.tex = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      texliveFull
      texlab
    ];

    modules.editors.neovim.lspconfig.servers = [
      { name = "texlab"; }
    ];
  };
}
