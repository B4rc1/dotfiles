{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.typescript;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.typescript = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs.nodePackages; [
      pkgs.nodejs
      pnpm pkgs.pnpm-shell-completion
      npm
      typescript
      typescript-language-server
      vscode-langservers-extracted
    ];

    modules.editors.neovim.lspconfig.servers = [
     { name = "cssls"; }
     {
      name = "ts_ls";
      # see https://github.com/typescript-language-server/typescript-language-server/issues/411#issuecomment-1065943942
      extraSetup =
      ''
      cmd = {
        "typescript-language-server",
        "--stdio",
      },
      '';
     }
    ];

  };
}
