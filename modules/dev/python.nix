{ config, options, lib, pkgs, inputs, system, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.python;
    configDir = config.dotfiles.configDir;
    mkPython = inputs.mach-nix.lib."${system}".mkPython;
in {
  options.modules.dev.python = {
    enable = mkBoolOpt false;
    jupyter.enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable
  {
    user.packages = with pkgs; [
      pyright
    ] ++ (if cfg.jupyter.enable == true then
    [
      vscode-fhs
      # (mkPython {
      #   # FIXME: Why is data always out of date?
      #   ignoreDataOutdated = true;
      #   providers = {
      #     _default = "nixpkgs,wheel,conda";
      #   };
      #   requirements = ''
      #     jupyterlab
      #     numpy
      #     matplotlib
      #   '';
      # })
      (pkgs.python3.withPackages (p: with p; [
        jupyterlab
        numpy
        matplotlib
        poetry-core
        pandas
      ]))
      poetry
    ]
    else
    [
      (pkgs.python3Full.withPackages (p: with p; [
        pandas numpy matplotlib
      ]))
      poetry
    ]);

    modules.editors.neovim.lspconfig.servers = [
     { name = "pyright"; }
    ];
  };
}
