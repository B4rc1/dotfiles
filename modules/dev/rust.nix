{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.rust;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.rust = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      unstable.rustc unstable.cargo unstable.rustfmt

      # lsp
      rust-analyzer
    ];

    modules.editors.neovim.lspconfig.servers = [
      { name = "rust_analyzer"; }
    ];
  };
}
