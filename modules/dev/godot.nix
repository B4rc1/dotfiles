{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.godot;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.godot = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      unstable.godot_4
      # (unstable.godot_4.overrideAttrs (final: prev: {
      #   version = "4.0.3-stable";
      #   src = fetchFromGitHub {
      #       owner = "godotengine";
      #       repo = "godot";
      #       rev = "5222a99f5d38cd5346254cefed8f65315bca4fcb";
      #       sha256 = "sha256-g9+CV3HsiJqiSJpZvK0N7BqKzp2Pvi6otjRLsFdmWGk=";
      #     };
      # }))
    ];

    modules.editors.neovim.lspconfig.servers = [
      { name = "gdscript";
        # extraSetup =
        # ''
        # flags = {
        #   debounce_text_changes = 150,
        # }
        # '';
      }
    ];
  };
}
