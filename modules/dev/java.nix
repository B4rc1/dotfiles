{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.java;
    configDir = config.dotfiles.configDir;
in {
  options.modules.dev.java = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      jdk
      jdt-language-server
    ];

    environment.variables = {
      JAVA_HOME = ''${pkgs.jdk}/lib/openjdk'';
    };

    modules.editors.neovim.lspconfig.servers = [
      {
        name = "jdtls";
        extraSetup =
        ''
          handlers = {}
        '';
      }
    ];
  };
}
