{ config, options, lib, pkgs, inputs, system,  ... }:

with lib;
with lib.my;
let cfg = config.modules.dev.sagemath;
    configDir = config.dotfiles.configDir;
    buildPythonPackage = inputs.mach-nix.lib."${system}".buildPythonPackage;
in {
  options.modules.dev.sagemath = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
      sage
      # (sage.override {
      #   # otherwise we will run all tests everytime we change anything about sage
      #   requireSageTests = false;
      #   # ignoreCollisions = true;
      #   # extraPythonPackages = ps:
      #   #   # FIXME: Why does this not work?
      #   #   # https://github.com/untitled-ai/jupyter_ascending.vim
        #   # https://github.com/untitled-ai/jupyter_ascending
        #
        #   (pkgs.poetry2nix.mkPoetryPackages {
        #     projectDir = builtins.fetchGit {
        #       url = "https://github.com/untitled-ai/jupyter_ascending";
        #       ref = "main";
        #       rev = "ab7ecec2654872726be1ebfa94900dc6745a7224";
        #     };
        #     preferWheels = true;
        #   }).poetryPackages;
          # [
            # (buildPythonPackage rec {
            #  pname = "jupyter_ascending";
            #  version = "0.1.23";
            #  format = "wheel";
            #
            #  src = ps.fetchPypi {
            #    inherit pname version;
            #    sha256 = "sha256-nXWc2zXb5c4dtsBpB9qhECs1fNki3PjYyoZrkb/2B1Q=";
            #  };
            # })
            # (buildPythonPackage {
            #   # For all these values see https://github.com/untitled-ai/jupyter_ascending/blob/main/setup.py
            #   pname = "jupyter_ascending";
            #   version = "0.1.23";
            #   src = builtins.fetchGit {
            #     url = "https://github.com/untitled-ai/jupyter_ascending";
            #     ref = "main";
            #     rev = "ab7ecec2654872726be1ebfa94900dc6745a7224";
            #   };
            #   requirements = ''
            #     ipywidgets>=7.0.0
            #   '';
            # })
          # ];
        # })
      # sage.kernelspec
      # sage.lib
      # sage.doc
    ];

    # home.configFile = {
    #   "example/example".source = "${configDir}/example/example";
    # };
  };
}
