{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.webcam;
    configDir = config.dotfiles.configDir;
in {
  options.modules.hardware.webcam = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    boot.extraModulePackages = with config.boot.kernelPackages; [ v4l2loopback ];

    environment.systemPackages = with pkgs; [
      v4l-utils
    ];
  };
}
