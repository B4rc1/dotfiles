{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.fs;
in {
  options.modules.hardware.fs = {
    enable = mkBoolOpt false;
    ssd.enable = mkBoolOpt false;
    sshfs.enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable (mkMerge [

    {
      programs.udevil.enable = true;

      environment.systemPackages = with pkgs; [
        exfat # windows drives
        ntfs3g # windows drives
      ];

      # see: https://wiki.archlinux.org/title/Udisks#Mount_to_/media_(udisks2)
      services.udev.extraRules = ''
        # UDISKS_FILESYSTEM_SHARED
        # ==1: mount filesystem to a shared directory (/media/VolumeName)
        # ==0: mount filesystem to a private directory (/run/media/$USER/VolumeName)
        # See udisks(8)
        ENV{ID_FS_USAGE}=="filesystem|other|crypto", ENV{UDISKS_FILESYSTEM_SHARED}="1"
      '';
    }

    (mkIf cfg.ssd.enable {
      services.fstrim.enable = true;
    })

    (mkIf (cfg.sshfs.enable) {
      services.gvfs.enable = true;

      environment.systemPackages = with pkgs; [
        sshfs
      ];

      systemd.services.backupPasswordDatabase = {
        description = "Backup pw.kdbx to /var/backup/";
        wants = [ "mnt-pi.mount" ];
        path = [ pkgs.rsync ];
        environment = {
          SRC = "/mnt/pi/home/nixos/share/pw.kdbx";
          DEST = "/var/backups/";
        };
        script = ''
          if [[ ! -d "$DEST" ]]; then
            mkdir "$DEST"
          fi

          echo "---- BACKUPING UP $SRC TO $DEST ----"
          rsync -tPl "$SRC" "$DEST"
        '';
        serviceConfig = {
          Type = "oneshot";
          Nice = 19;
          IOSchedulingClass = "idle";
        };
      };

      systemd.timers.backupPasswordDatabase = {
        wantedBy = [ "timers.target" ];
        partOf = [ "backupPasswordDatabase.service" ];
        timerConfig.OnCalendar = "weekly";
        timerConfig.Persistent = true;
      };

      fileSystems."/mnt/pi" = {
        device = "nixos@192.168.178.77:/";
              fsType = "fuse.sshfs";
              # this line prevents hanging on network split
              options = [
                "_netdev"
                "noauto,x-systemd.automount,x-systemd.idle-timeout=60,x-systemd.device-timeout=5s,x-systemd.mount-timeout=5s"
                "allow_other,default_permissions"
                "async"
                "identityfile=${config.user.home}/.ssh/id_rsa"
              ];
            };
      })
  ]);
}
