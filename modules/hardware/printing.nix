{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.printing;
    configDir = config.dotfiles.configDir;
in {
  options.modules.hardware.printing = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.printing.enable = true;
    services.printing.browsing = true;
    services.printing.drivers = with pkgs; [
      gutenprint gutenprintBin brlaser brgenml1lpr brgenml1cupswrapper
    ];
    services.avahi.enable = true;
    services.avahi.nssmdns4 = true;
  };
}
