{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.yubikey;
    configDir = config.dotfiles.configDir;
in {
  options.modules.hardware.yubikey = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    services.udev.packages = [ pkgs.yubikey-personalization ];

    # For Personal Indentity Verification (PIV)
    # services.pcscd.enable = true;

    hardware.gpgSmartcards.enable = true;


    environment.systemPackages = with pkgs; [
      yubikey-manager-qt

      yubikey-personalization

      yubioath-flutter
    ];

    programs.gnupg.agent = {
      enable = true;
    };
  };
}
