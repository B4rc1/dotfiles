{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;

let
  cfg = config.modules.hardware.bluetooth;
in {
  options.modules.hardware.bluetooth = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable (mkMerge [
    {
      hardware.bluetooth.enable = true;

      hardware.enableAllFirmware = true;

      environment.systemPackages = [ pkgs.blueman ];

      services.dbus.packages = [ pkgs.blueman ];

      systemd.packages = [ pkgs.blueman ];
    }
  ]);
}
