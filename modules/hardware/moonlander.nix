{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.moonlander;
    configDir = config.dotfiles.configDir;
in {
  options.modules.hardware.moonlander = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    user.packages = with pkgs; [
        unstable.wally-cli
        qmk
        # This is a small wrapper i wrote to integate oryx into my c source changes.
        # see https://codeberg.org/B4rc1/qmk-B4rc1/src/branch/B4rc1/keyboards/moonlander/keymaps/B4rc1/apply-keyboard.sh
        # for the whole story
        (pkgs.writeShellScriptBin "apply-keyboard.sh" ''
          if [[ $# -ne 1 || ! -f "$1" ]]; then
            echo usage: $0 [zip to install]
            exit 1
          fi
          file=$(realpath "$1")
          cd ~/nerdshit/kbds/qmk_firmware_zsa/
          nix-shell --command "sh -c \"./keyboards/moonlander/keymaps/B4rc1/apply-keyboard.sh \"$file\" &&\
            qmk compile &&\
            qmk flash\
        \""
        '')

    ];

    hardware.keyboard.zsa.enable = true;

    user.extraGroups = [ "plugdev" ];

    home.configFile = {
      "qmk".source = "${configDir}/qmk";
    };

    # since this needs to be mutable, we just copy it if it does not exist.
    system.userActivationScripts.cloneQMK = ''
      qmk_dir=/home/${config.user.name}/nerdshit/kbds/qmk_firmware_zsa
      if [[ ! -d $qmk_dir ]]; then
        mkdir -p $qmk_dir
        ${pkgs.git}/bin/git clone --recurse-submodules git@codeberg.org:B4rc1/qmk-B4rc1.git $qmk_dir
      fi
    '';
  };
}
