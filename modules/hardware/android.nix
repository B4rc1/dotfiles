{ config, options, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.hardware.android;
    configDir = config.dotfiles.configDir;
in {
  options.modules.hardware.android = {
    enable = mkBoolOpt false;
  };

  config = mkIf cfg.enable {
    programs.adb.enable = true;
    user.extraGroups = [ "adbusers" ];
    environment.systemPackages = with pkgs; [
      jmtpfs
    ];
  };
}
