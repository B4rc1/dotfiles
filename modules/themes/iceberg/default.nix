{ options, config, lib, pkgs, ... }:

with lib;
with lib.my;
let cfg = config.modules.theme;
in {
  config = mkIf (cfg.active == "iceberg") (mkMerge [
    # Desktop-agnostic configuration
    {
      modules = {
        theme = {
          gtk = {
            theme = "Dracula";
            iconTheme = "Paper";
            cursorTheme = "Paper";
          };
        };

        shell.zsh.rcFiles  = [ ./config/zsh/prompt.zsh ];
        /* desktop.browsers = { */
        /*   firefox.userChrome = concatMapStringsSep "\n" readFile [ */
        /*     ./config/firefox/userChrome.css */
        /*   ]; */
        /* }; */
      };
    }

    # Desktop (X11) theming
    # FIXME: better detect wayland, other than xwayland
    (mkIf (config.services.xserver.enable || programs.xwayland.enable) {
      user.packages = with pkgs; [
        unstable.dracula-theme
        paper-icon-theme # for rofi
      ];
      fonts = {
        packages = with pkgs; [
          fira-code
          noto-fonts
          fira-code-symbols
          jetbrains-mono
          siji
          font-awesome
        ];
        fontconfig.defaultFonts = {
          sansSerif = ["Fira Sans"];
          monospace = ["Fira Code"];
        };
      };

      # Other dotfiles
      home.configFile = with config.modules; mkMerge [
        {
          # Sourced from sessionCommands in modules/themes/default.nix
          "xtheme/90-theme".source = ./config/Xresources;
        }
        (mkIf desktop.apps.rofi.enable {
          "rofi/theme" = { source = ./config/rofi; recursive = true; };
        })
        (mkIf (desktop.dwm.enable || desktop.herbstluftwm.enable) {
          /* "dunst/dunstrc".source = ./config/dunstrc; */
        })
        (mkIf desktop.herbstluftwm.enable {
          "polybar/my-bar".source = ./config/polybar/my-bar;
          "herbstluftwm/theme".source = ./config/herbstluftwm/theme;
        })
      ];
    })
  ]);
}
